Storage module
===================
Add file(s) in model

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist northug/yii2-storage "*"
```

or add

```
"northug/yii2-storage": "*"
```

to the require section of your `composer.json` file.

Add config

```
'modules' => [
    ...
    'storage' => [
        'class' => 'northug\storage\Module',
        //'homeUrl' => 'http://domain.com',
    ],
    ...
],
```

```php
yii migrate --migrationPath=@northug/storage/migrations
```