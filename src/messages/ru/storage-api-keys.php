<?php

return [
    'Api Keys' => 'Api ключи',
    'Save' => 'Сохранить',
    'Update' => 'Редактировать',
    'Delete' => 'Удалить',
    'Update Api Key: {key}' => 'Редактирование api ключа: {key}',
    'Update key' => 'Редактирование ключа',
    'Create new key' => 'Добавить новый ключ',
    'Creating key' => 'Добавление ключа',
    'Settings' => 'Настройки',
];
