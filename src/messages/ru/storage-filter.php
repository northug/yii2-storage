<?php

return [
    'Path' => 'Путь',
    'File name before downloading' => 'Название файла до загрузки',
    'Expansion' => 'Расширение',
    'Width' => 'Ширина',
    'Height' => 'Высота',
    'Size' => 'Размер',
    'Size from (in Mb)' => 'Размер от (в Mb)',
    'Size up to (in Mb)' => 'Размер до (в Mb)',
    'Squeeze' => 'Сжатие',
    'Downloaded from ip' => 'Загружено с Ip',
    'Date and time of editing' => 'Дата и время редактирования',
    'Date of adding' => 'Дата добавления',
    'Date added from' => 'Дата добавления от',
    'Uploaded to' => 'Дата добавления до',
    'Search' => 'Искать',
    'Reset' => 'Сброс',
    'Select a category(s)...' => 'Выберите категорию(и)',
    'Categories' => 'Категории',
    'Compressed files' => 'Сжатые файлы'
];
