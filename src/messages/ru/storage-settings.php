<?php

return [
    'Submit' => 'Сохранить',
    'Api Key Tiny' => 'API-ключ от сайта tinypng.com',
    'Number of compressed files' => 'Количество сжатых файлов',
    'Spots saved' => 'Сэкономлено места',
    'Compression on loading' => 'Сжатие при загрузке',
    'Storage settings' => 'Настройки хранилища',
    'List of keys from the site tinypng.com' => 'Список ключей с сайта tinypng.com',
];
