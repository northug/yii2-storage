<?php

return [
    'Name category' => 'Название категории',
    'Status' => 'Активность',
    'Sort' => 'Порядок сортировки',
    'Categories' => 'Категории',
    'Create Categories' => 'Создание категории',
    'Create categories' => 'Создать категорию',
    'Save' => 'Сохранить',
    'Update' => 'Редактирование',
    'Update Categories: {name}' => 'Редактирование категории: {name}',
    'Update category' => 'Редактировать категорию',
    'Delete' => 'Удалить',
    'Active' => 'Активно',
    'Not active' => 'Не активно',
    'Select a category(s) or create a new one...' => 'Выберите категорию(и) или создайте новую',
    'Categories (for new files)' => 'Категории (для новых файлов)',
    'Settings' => 'Настройки',
];