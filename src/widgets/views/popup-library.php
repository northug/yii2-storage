<?php
/* @var $searchModel northug\storage\models\_search\StorageSearch */
/* @var $categories northug\storage\models\Categories */
?>
<div id="popup-library" class="col-md-12 row">
    <div class="col-md-9 row vh96">
        <div class="col-md-12 row list-files-popup">

        </div>
    </div>
    <div class="col-md-3 vh96">
        <?=
        $this->render('_search', [
            'model' => $searchModel,
            'categories' => $categories
        ]);
        ?>
    </div>
    <div class="col-md-12 row">
        <button class="btn btn-success col-md-9 row add-library">Добавить</button>
        <button class="btn btn-danger col-md-3 close-library">Закрыть</button>
    </div>
</div>