<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use northug\storage\Module;
use kartik\select2\Select2;
use kartik\base\AssetBundle;
use northug\storage\assets\MainAsset;
use kartik\sortinput\SortableInput;
use yii\helpers\FileHelper;

MainAsset::register($this);

/* @var $name string */
/* @var $formName string */
/* @var $nameFilesInStorage string */
/* @var $title string */
/* @var statusButton boolean */
/* @var $attribute string */
/* @var $nameCategories string */
/* @var $this yii\web\View */
/* @var $categories northug\storage\models\Categories */
/* @var $files northug\storage\models\Storage */
/* @var $searchModel northug\storage\models\_search\StorageSearch */

Yii::$app->assetManager->bundles['kartik\select2\Select2Asset'] = [
    //'js' => AssetBundle::EMPTY_ASSET,
    'css' => AssetBundle::EMPTY_ASSET,
];

//Yii::$app->assetManager->bundles['kartik\sortinput\SortableInput'] = [
//    //'js' => AssetBundle::EMPTY_ASSET,
//    'css' => AssetBundle::EMPTY_ASSET,
//];
//print_r(Yii::$app->assetManager->bundles);exit;
$this->registerCssFile('/static/css/separate/vendor/bootstrap-select/bootstrap-select.min.css');
$this->registerCssFile('/static/css/separate/vendor/select2.min.css');

$this->registerCssFile('/static/js/lib/bootstrap-select/bootstrap-select.min.js');
$this->registerCssFile('/static/js/lib/select2/select2.full.min.js');
?>

<div class="file-widget-block m-b-md p-a-lg" data-attribute="<?= $attribute ?>">
    <?php
    if ($title) {
        echo Html::tag('h4', $title);
    }

    echo Html::beginTag('div', ['class' => 'storage-file-input col-md-12 row']);
    echo Html::input('file', $name, null, [
        'multiple' => true,
        'class' => 'input-new-file'
    ]);
    if ($statusButton) {
        echo Html::a(Module::t('storage', 'File Library'), false, [
            'class' => 'btn btn-primary open-library',
            'data-attribute' => $attribute,
            'data-name' => $formName,
        ]);
    }
    echo Html::endTag('div')
    ?>

    <?php
    if ($categories !== null) {
        echo Html::beginTag('div', ['class' => 'm-t']);
        echo '<label class="control-label">' . Module::t('storage-category', 'Categories (for new files)') . '</label>';
        echo Select2::widget([
            'name' => $nameCategories,
            'theme' => Select2::THEME_DEFAULT,
            //'value' => ['red', 'green'], // initial value
            'data' => $categories,
            'maintainOrder' => true,
            'showToggleAll' => false,
            'options' => [
                'placeholder' => Module::t('storage-category', 'Select a category(s) or create a new one...'),
                'multiple' => true,
                'class' => 'select2'
            ],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 25,
            ],
        ]);
        echo Html::endTag('div');
    }


    if ($files) {
        $data = [
            'name' => $name . '-' . $attribute,
            'items' => [],
            'hideInput' => true,
            'options' => ['class' => 'form-control', 'readonly' => true, 'id' => $attribute],
            'sortableOptions' => ['type' => 'grid', 'options' => ['class' => 'list-files'],]
        ];
        $currentHost = Yii::$app->request->headers->get('host');
        $host = substr($currentHost, strpos($currentHost, ".") + 1, strlen($currentHost));
        $homeUrl = Yii::$app->modules['storage'] ? Yii::$app->modules['storage']['homeUrl'] : null;

        foreach ($files as $keyFile => $file) {

            $url = $homeUrl ? $homeUrl . $file->path : 'http://' . $host . $file->path;
            
            $content = '';

            $content .= Html::beginTag('div', ['class' => 'file-buttons']);
            $content .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                        'class' => 'fancybox',
                        'data-fancybox' => $attribute
            ]);
            $content .= Html::a('<span class="glyphicon glyphicon-copy"></span>', false, [
                        'title' => 'Скопировать путь в буфер обмена',
                        'aria-label' => 'Сохранить путь в буфер обмена',
                        'id' => 'fileClipboard-save-path' . $file->id,
                        'data-clipboard-text' => $url
            ]);
            $content .= Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['storage/default/update', 'id' => $file->id]), [
                        'title' => 'Редактировать',
                        'class' => 'a-glyphicon-pencil',
                        'target' => '_blank',
                        'aria-label' => 'Редактировать',
            ]);
            $content .= Html::tag('span', false, [
                        'class' => 'glyphicon glyphicon-trash delete-file',
                        'data-id' => $file->id,
                        'data-post' => $attribute
            ]);
            $content .= Html::endTag('div');
            $content .= Html::input('hidden', $nameFilesInStorage, $file->id, ['class' => 'input-id']);

            if (!$file->is_image() and $file->expansion != 'svg') {
                $content .= Html::tag('span', '.' . $file->expansion);
            }
            
            $data['items'][] = ['content' => $content, 'options' => [
                'style' => ($file->is_image() or $file->expansion == 'svg') ? 'background-image: url("' . $url . '");' : null
            ]];
            $this->registerJs("new Clipboard('#fileClipboard-save-path" . $file->id . "');", View::POS_READY);
        }

        echo SortableInput::widget($data);
    }
    ?>
</div>
<?php
if ($statusButton) {
    require_once FileHelper::normalizePath(__DIR__ . '/popup-library.php');
}
