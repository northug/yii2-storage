<?php

use northug\storage\Module;
use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\select2\Select2;

//use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model northug\storage\models\_search\StorageSearch */
/* @var $form yii\widgets\ActiveForm */

$formName = $model->formName();
?>

<div class="storage-search">

    <?php
    echo Html::beginTag('div', ['id' => 'filter-storage-form', 'class' => 'filter-storage-form']);

    echo Html::csrfMetaTags();

    echo Html::tag('label', Module::t('storage-filter', 'ID'), [
        'class' => 'form-label semibold'
    ]);
    echo Html::tag('div', Html::input('number', $formName . '[id]', null, [
        'class' => 'form-control'
    ]), [
        'class' => 'form-group'
    ]);

    echo Html::tag('label', Module::t('storage-filter', 'Path'));
    echo Html::tag('div', Html::input('text', $formName . '[path]', null, [
        'class' => 'form-control'
    ]), [
        'class' => 'form-group'
    ]);

    echo Html::beginTag('div', ['class' => 'form-group m-t']);
    echo '<label class="control-label">' . Module::t('storage-filter', 'Categories') . '</label>';
    echo Select2::widget([
        'name' => $formName . '[categories]',
        'theme' => Select2::THEME_DEFAULT,
        'data' => $categories,
        'maintainOrder' => true,
        'showToggleAll' => false,
        'options' => [
            'placeholder' => Module::t('storage-filter', 'Select a category(s)...'),
            'multiple' => true,
            'class' => 'select2'
        ],
        'pluginOptions' => [
            'tags' => false,
            'maximumInputLength' => 25,
        ],
    ]);
    echo Html::endTag('div');

    echo Html::tag('label', Module::t('storage-filter', 'File name before downloading'));
    echo Html::tag('div', Html::input('text', $formName . '[old_name]', null, [
        'class' => 'form-control'
    ]), [
        'class' => 'form-group'
    ]);

    echo Html::tag('label', Module::t('storage-filter', 'Expansion'));
    echo Html::tag('div', Html::input('text', $formName . '[expansion]', null, [
        'class' => 'form-control'
    ]), [
        'class' => 'form-group'
    ]);


    echo Html::tag('div', Html::tag('label', Module::t('storage-filter', 'Width')) . Html::input('text', $formName . '[width]', null, [
        'class' => 'form-control'
    ]), [
        'class' => 'form-group col-md-6 p-l-0'
    ]);

    echo Html::tag('div', Html::tag('label', Module::t('storage-filter', 'Height')) . Html::input('text', $formName . '[height]', null, [
        'class' => 'form-control'
    ]), [
        'class' => 'form-group col-md-6 p-r-0'
    ]);

    echo Html::tag('div', Html::tag('label', Module::t('storage-filter', 'Size from (in Mb)')) . Html::input('number', $formName . '[after_size]', null, [
        'class' => 'form-control'
    ]), [
        'class' => 'form-group col-md-6 p-l-0'
    ]);

    echo Html::tag('div', Html::tag('label', Module::t('storage-filter', 'Size up to (in Mb)')) . Html::input('number', $formName . '[before_size]', null, [
        'class' => 'form-control'
    ]), [
        'class' => 'form-group col-md-6 p-r-0'
    ]);

    echo Html::tag('div', Html::tag('label', Module::t('storage-filter', 'Squeeze'), [
                'for' => 'storage-filter-checkbox'
            ]) . Html::input('checkbox', $formName . '[squeeze]', null, [
                'id' => 'storage-filter-checkbox'
            ]), [
        'class' => 'form-group storage-filter-checkbox',
    ]);

    echo Html::tag('label', Module::t('storage-filter', 'Downloaded from ip'), [
        'class' => 'form-label semibold'
    ]);
    echo Html::tag('div', Html::input('text', $formName . '[upload_ip]', null, [
        'class' => 'form-control'
    ]), [
        'class' => 'form-group'
    ]);

    echo Html::beginTag('div', ['class' => 'form-group']);
    echo Html::tag('label', Module::t('storage-filter', 'Date added from'));
    echo DatePicker::widget([
        'name' => $formName . '[after_created_at]',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy',
        ],
    ]);
    echo Html::endTag('div');

    /*
      echo Html::beginTag('div', ['class' => 'form-group']);
      echo Html::tag('label', Module::t('storage-filter', 'Date of adding'));
      echo DatePicker::widget([
      'name' => $formName . '[created_at]',
      'pluginOptions' => [
      'autoclose'=>true,
      'format' => 'dd.mm.yyyy',
      ],
      ]);
      echo Html::endTag('div');
     * 
     */

    echo Html::beginTag('div', ['class' => 'form-group']);
    echo Html::tag('label', Module::t('storage-filter', 'Uploaded to'));
    echo DatePicker::widget([
        'name' => $formName . '[before_created_at]',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy',
        ],
    ]);
    echo Html::endTag('div');

    echo Html::endTag('div');
    ?>

    <div class="form-group">
        <?= Html::submitButton(Module::t('storage-filter', 'Search'), ['class' => 'btn btn-primary storage-search']) ?>
        <?= Html::resetButton(Module::t('storage-filter', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

</div>
