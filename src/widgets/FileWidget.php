<?php

namespace northug\storage\widgets;

use Yii;
use northug\storage\Module;
use northug\storage\models\_search\FilterSearch;
use kartik\base\InputWidget;
use northug\storage\models\Categories;
use northug\storage\models\StorageToModel;
use northug\storage\models\form\FileForm;

/**
 * Description of FileUpload
 *
 * @author Пользователь
 */
class FileWidget extends InputWidget {
    
    const NAME_CATEGORIES = 'categories';
    
    public $name = 'files';
    public $nameFilesInStorage;
    public $nameCategories = null;
    public $formName = null;
    public $inputName = null;
    
    /**
     * Default title
     * @var string 
     */
    public $title = 'Select file(s)';
    
    /**
     * Displaying a list of categories
     * @var boolean 
     */
    public $statusCategory = true;
    
    /**
     * Displaying a button with a repository
     * @var boolean 
     */
    public $statusButton = true;
    
    /**
     * List categories
     * @var array 
     */
    public $categories = null;
    
    /**
     * Primary domain
     * @var string 
     */
    public $mainDomain = null;
    
    /**
     * List of current files in the attribute
     * @var Storage 
     */
    private $files;
    
    /**
     * A model for searching the repository
     * @var FilterSearch 
     */
    private $searchModel;
    
    public function init() {
        if ($this->formName === null) {
            $this->formName = (new FileForm())->formName();
        }
        if ($this->model) {
            $this->formName = $this->model->formName();
            $this->inputName = $this->formName.'['.$this->attribute.']';
        } elseif ($this->inputName === null) {
            $this->inputName = $this->formName.'['.$this->name.']';
        }
        if ($this->title === 'Select file(s)') {
            $this->title = Module::t('storage', $this->title);
        } 
        if ($this->statusCategory) {
            $this->categories = Categories::getAllSelect();
            $this->nameCategories = $this->inputName.'['.self::NAME_CATEGORIES.']';
        }
        $this->inputName .= '[]';
        if ($this->model and $this->attribute) {
            $this->files = StorageToModel::getFilesByAttribute($this->model, $this->attribute);
            $this->nameFilesInStorage = $this->formName.'[storage-'.$this->attribute.'][]';
        }
        $this->mainDomain = $this->getMainDomain();
        $this->searchModel = new FilterSearch();
    }
    
    public function run() {
        return $this->render('file-upload', [
            'name' => $this->inputName,
            'nameFilesInStorage' => $this->nameFilesInStorage,
            'title' => $this->title,
            'categories' => $this->categories,
            'nameCategories' => $this->nameCategories,
            'files' => $this->files,
            'attribute' => $this->attribute,
            'mainDomain' => $this->mainDomain,
            'statusButton' => $this->statusButton,
            'formName' => $this->formName,
            'searchModel' => $this->searchModel,
        ]);
    }
    
    /**
     * Returns the primary domain of the site
     * @return string
     */
    private function getMainDomain()
    {
        if (!$this->mainDomain) {
            $currentDomain = Yii::$app->request->hostInfo;
            $arr = explode('//', $currentDomain);
            return $arr[0].'//'.$this->giveHost($currentDomain);;
        }
        return $this->mainDomain;
    }
    
    private function giveHost($host_with_subdomain) {
        $array = explode(".", $host_with_subdomain);
        return (array_key_exists(count($array) - 2, $array) ? $array[count($array) - 2] : "").".".$array[count($array) - 1];
    }
    
}
