<?php

use yii\db\Migration;

/**
 * Handles the creation of table `storage`.
 */
class m180814_102002_create_storage_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('storage', [
            'id' => $this->primaryKey(),
            'path' => $this->string(255)->notNull(),
            'absolutePath' => $this->string(255)->defaultValue('@frontend/web'),
            'alt' => $this->string(255),
            'old_name' => $this->string(255)->defaultValue(null),
            'type_file' => $this->string(255)->defaultValue(null),
            'expansion' => $this->string(6)->notNull(),
            'width' => $this->integer()->defaultValue(null),
            'height' => $this->integer()->defaultValue(null),
            'size' => $this->integer()->defaultValue(null),
            'squeeze' => $this->boolean()->defaultValue(0),
            'upload_ip' => $this->string(15)->defaultValue(null),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('storage');
    }
}
