<?php

use yii\db\Migration;

/**
 * Handles the creation of table `storage_category`.
 */
class m180815_101720_create_storage_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('storage_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->unique(),
            'status' => $this->boolean()->defaultValue(1),
            'sort' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('storage_category');
    }
}
