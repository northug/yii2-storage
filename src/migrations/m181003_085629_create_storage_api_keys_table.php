<?php

use yii\db\Migration;

/**
 * Handles the creation of table `storage_api_keys`.
 */
class m181003_085629_create_storage_api_keys_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('storage_api_keys', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('storage_api_keys');
    }
}
