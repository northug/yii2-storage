<?php

use yii\db\Migration;

/**
 * Class m181003_084844_delete_column_api_key_tiny_from_storage_settings_table
 */
class m181003_084844_delete_column_api_key_tiny_from_storage_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%storage_settings}}', 'api_key_tiny');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%storage_settings}}', 'api_key_tiny', $this->string(255));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181003_084844_delete_column_api_key_tiny_from_storage_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
