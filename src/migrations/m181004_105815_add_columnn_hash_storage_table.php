<?php

use yii\db\Migration;

/**
 * Class m181004_105815_add_columnn_hash_storage_table
 */
class m181004_105815_add_columnn_hash_storage_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('storage', 'hash', $this->string(255).' AFTER `id`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('storage', 'hash');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181004_105815_add_columnn_hash_storage_table cannot be reverted.\n";

        return false;
    }
    */
}
