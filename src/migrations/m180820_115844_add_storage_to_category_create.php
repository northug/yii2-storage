<?php

use yii\db\Migration;

/**
 * Class m180820_115844_add_storage_to_category_create
 */
class m180820_115844_add_storage_to_category_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('storage_to_category', [
            'id' => $this->primaryKey(),
            'storage_id' => $this->integer(),
            'category_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('storage_to_category');
    }

}
