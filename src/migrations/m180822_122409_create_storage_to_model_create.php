<?php

use yii\db\Migration;

/**
 * Class m180822_122409_create_storage_to_model_create
 */
class m180822_122409_create_storage_to_model_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('storage_to_model', [
            'id' => $this->primaryKey(),
            'model' => $this->string(255),
            'attribute' => $this->string(255),
            'model_id' => $this->integer(),
            'file_id' => $this->integer(),
            'sort' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('storage_to_model');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180822_122409_create_storage_to_model_create cannot be reverted.\n";

        return false;
    }
    */
}
