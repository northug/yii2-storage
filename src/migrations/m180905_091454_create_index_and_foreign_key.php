<?php

use yii\db\Migration;

/**
 * Class m180905_091454_create_index_and_foreign_key
 */
class m180905_091454_create_index_and_foreign_key extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createIndex(
                'idx-storage_to_category-storage_id', 'storage_to_category', 'storage_id'
        );
        $this->addForeignKey(
                'idx-storage_to_category-storage_id', 'storage_to_category', 'storage_id', 'storage', 'id', 'CASCADE'
        );

        $this->createIndex(
                'idx-storage_to_category-category_id', 'storage_to_category', 'category_id'
        );
        $this->addForeignKey(
                'idx-storage_to_category-category_id', 'storage_to_category', 'category_id', 'storage_category', 'id', 'CASCADE'
        );

        $this->createIndex(
                'idx-storage_to_model-file_id', 'storage_to_model', 'file_id'
        );
        $this->addForeignKey(
                'idx-storage_to_model-file_id', 'storage_to_model', 'file_id', 'storage', 'id', 'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropForeignKey('idx-storage_to_model-file_id', 'storage_to_model');
        $this->dropForeignKey('idx-storage_to_category-category_id', 'storage_to_category');
        $this->dropForeignKey('idx-storage_to_category-storage_id', 'storage_to_category');
        
        $this->dropIndex('idx-storage_to_model-file_id', 'storage_to_model');
        $this->dropIndex('idx-storage_to_category-category_id', 'storage_to_category');
        $this->dropIndex('idx-storage_to_category-storage_id', 'storage_to_category');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180905_091454_create_index_and_foreign_key cannot be reverted.\n";

      return false;
      }
     */
}
