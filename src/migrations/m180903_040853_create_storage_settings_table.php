<?php

use yii\db\Migration;

/**
 * Handles the creation of table `storage_settings`.
 */
class m180903_040853_create_storage_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('storage_settings', [
            'id' => $this->primaryKey(),
            'api_key_tiny' => $this->string(255),
            'storage_sum_files' => $this->integer(),
            'storage_sum_size' => $this->decimal(10.2),
            'status_squeeze' => $this->boolean()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('storage_settings');
    }
}
