<?php

use yii\db\Migration;

/**
 * Handles the creation of table `storage_resize_files`.
 */
class m181004_083954_create_storage_resize_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('storage_resize_files', [
            'id' => $this->primaryKey(),
            'absolutePath' => $this->text()->notNull(),
            'hash' => $this->string(255)->notNull()->unique(),
            'squeeze' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('storage_resize_files');
    }
}
