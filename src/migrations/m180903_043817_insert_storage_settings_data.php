<?php

use yii\db\Migration;

/**
 * Class m180903_043817_insert_storage_settings_data
 */
class m180903_043817_insert_storage_settings_data extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        Yii::$app->db->createCommand()->batchInsert('storage_settings', [
            'api_key_tiny',
            'storage_sum_files',
            'storage_sum_size',
            'status_squeeze',
                ], [
            [
                '',
                0,
                0,
                0,
            ],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->truncateTable('storage_settings');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180903_043817_insert_storage_settings_data cannot be reverted.\n";

      return false;
      }
     */
}
