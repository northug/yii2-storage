<?php

namespace northug\storage;

use Yii;

/**
 * storage module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'northug\storage\controllers';
    
    public $homeUrl;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }
    
    public static function t($category, $message, $params = [], $language = null) 
    {
        Yii::$app->i18n->translations['storage*'] = [
            'class'          => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath'       => '@northug/storage/messages',
        ];
        return Yii::t($category, $message, $params, $language); 
    } 

}
