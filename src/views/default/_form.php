<?php

use yii\helpers\Html;
use northug\storage\Module;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use kartik\select2\Select2;
use kartik\base\AssetBundle;

/* @var $this yii\web\View */
/* @var $model northug\storage\models\Storage */
/* @var $form yii\widgets\ActiveForm */

Yii::$app->assetManager->bundles['kartik\select2\Select2Asset'] = [
    //'js' => AssetBundle::EMPTY_ASSET,
    'css' => AssetBundle::EMPTY_ASSET,
];

$this->registerCssFile('/static/css/separate/vendor/bootstrap-select/bootstrap-select.min.css');
$this->registerCssFile('/static/css/separate/vendor/select2.min.css');

$this->registerCssFile('/static/js/lib/bootstrap-select/bootstrap-select.min.js');
$this->registerCssFile('/static/js/lib/select2/select2.full.min.js');
//print_r($model->categoriesList);exit;
?>

<div class="storage-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'alt')->textInput(['maxlength' => true]) ?>

    <?php
    $model->categories = $model->getCategoriesListSelect();
    echo $form->field($model, 'categories')->widget(Select2::classname(), [
        'data' => $model->getCategoriesSelect(),
        'theme' => Select2::THEME_DEFAULT,
        'options' => [
            'placeholder' => Module::t('storage-category', 'Select a category(s) or create a new one...'),
            'multiple' => true,
            'class' => 'select2'
        ],
        'showToggleAll' => false,
        'pluginOptions' => [
            'tags' => true,
            'maximumInputLength' => 25
        ],
    ])->label(Module::t('storage-category', 'Categories'));
    ?>

    <br>
    <?= Html::tag('h5', Module::t('storage', 'Information')) ?>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'path',
            'absolutePath',
            'alt',
            'old_name',
            'expansion',
            'width',
            'height',
            [
                'attribute' => 'size',
                'format' => 'raw',
                'value' => function ($data) {
                    return Yii::$app->formatter->asShortSize($data->size);
                }
            ],
            [
                'attribute' => 'squeeze',
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data->squeeze) {
                        return Module::t('storage', 'Done');
                    }
                    return Module::t('storage', 'Not done');
                }
            ],
            'upload_ip',
            'updated_at:datetime',
            'created_at:datetime',
        ],
    ])
    ?>
    <br>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('storage', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
