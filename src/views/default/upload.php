<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use northug\storage\Module;
use northug\storage\widgets\FileWidget;

/* @var $this yii\web\View */
/* @var $model northug\storage\models\Storage */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="storage-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <?= FileWidget::widget([
        'statusButton' => false
    ]) ?>
    
    <div class="form-group">
        <?= Html::submitButton(Module::t('storage', 'Upload'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
