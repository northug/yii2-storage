<?php

use yii\helpers\Html;
use northug\storage\Module;
use yii\widgets\Breadcrumbs;


/* @var $this yii\web\View */
/* @var $model northug\storage\models\Storage */

$this->title = Module::t('storage', 'Adding a file(s)');
$this->params['breadcrumbs'][] = ['label' => Module::t('storage', 'Storage'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="storage-create">

    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
            ]); ?>
    </div>
    
    <?php if (Yii::$app->session->hasFlash('successUploadFiles')): ?>
        <div class="col-xl-12 col-lg-12 no-padding">
            <div class="alert alert-success alert-fill alert-close alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <?php echo \Yii::$app->session->getFlash('successUploadFiles'); ?>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if (Yii::$app->session->hasFlash('errorUploadFiles')): ?>
        <div class="col-xl-12 col-lg-12 no-padding">
            <div class="alert alert-danger alert-fill alert-close alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <?php echo \Yii::$app->session->getFlash('errorUploadFiles'); ?>
            </div>
        </div>
    <?php endif; ?>

    <?= $this->render('upload', [
        'model' => $model,
    ]) ?>

</div>
