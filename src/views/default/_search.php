<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use northug\storage\Module;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model northug\storage\models\_search\StorageSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $categories array */

?>

<div class="storage-search hide-storage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', [
        'options' => ['class' => 'col-md-1 p-l-0']
    ]) ?>

    <?= $form->field($model, 'path', [
        'options' => ['class' => 'col-md-11 p-l-0']
    ]) ?>
    
    <?= $form->field($model, 'categories')->widget(Select2::className(), [
        'theme' => Select2::THEME_DEFAULT,
        'data' => $categories,
        'maintainOrder' => true,
        'showToggleAll' => false,
        'options' => [
            'placeholder' => Module::t('storage-filter', 'Select a category(s)...'),
            'multiple' => true,
            'class' => 'select2'
        ],
        'pluginOptions' => [
            'tags' => false,
            'maximumInputLength' => 25,
        ],
    ]) ?>
    
    <?= $form->field($model, 'upload_ip') ?>

    <?= $form->field($model, 'alt') ?>

    <?= $form->field($model, 'old_name', [
        'options' => ['class' => 'col-md-6 p-l-0']
    ]) ?>

    <?= $form->field($model, 'expansion', [
        'options' => ['class' => 'col-md-6 p-r-0']
    ]) ?>

    <?= $form->field($model, 'width', [
        'options' => ['class' => 'col-md-6 p-l-0']
    ]) ?>

    <?= $form->field($model, 'height', [
        'options' => ['class' => 'col-md-6 p-r-0']
    ]) ?>

    <?php // echo $form->field($model, 'size') ?>
    
    <?= $form->field($model, 'after_size', [
        'options' => ['class' => 'col-md-6 p-l-0']
    ]) ?>
    
    <?= $form->field($model, 'before_size', [
        'options' => ['class' => 'col-md-6 p-r-0']
    ]) ?>

    <?= $form->field($model, 'squeeze', [
            'template' => "<label>{input} ".Module::t('storage-filter', 'Compressed files')."</label> {error}",
    ])->checkbox([
                'id' => 'compressed-checkbox',
        ], false)->label(Module::t('storage-filter', 'Compressed files'),[
                'for' => 'compressed-checkbox',
                ]) ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?= $form->field($model, 'after_created_at', [
        'options' => ['class' => 'col-md-6 p-l-0']
    ])->widget(DatePicker::className(), [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy',
        ],
    ]) ?>

    <?= $form->field($model, 'before_created_at', [
        'options' => ['class' => 'col-md-6 p-r-0']
    ])->widget(DatePicker::className(), [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy',
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Module::t('storage-filter', 'Search'), ['class' => 'btn btn-success']) ?>
        <?= Html::resetButton(Module::t('storage-filter', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
