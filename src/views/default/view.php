<?php

use yii\helpers\Html;
use northug\storage\Module;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model northug\storage\models\Storage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Module::t('storage', 'Storage'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storage-view">

    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
            ]); ?>
    </div>

    <p>
        <?php
        if (!$model->squeeze and $model->type_file == $model::TYPE_FILE_IMAGE) {
            echo Html::a(Module::t('storage', 'Compress'), ['compress', 'id' => $model->id], ['class' => 'btn btn-success m-r']);
        }
        echo Html::a(Module::t('storage', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary m-r']);
        echo Html::a(Module::t('storage', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger m-r',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]); ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'path',
            'absolutePath',
            'alt',
            'old_name',
            'expansion',
            'width',
            'height',
            [
                'attribute' => 'size',
                'format' => 'raw',
                'value' => function ($data) {
                    return Yii::$app->formatter->asShortSize($data->size);
                }
            ],
            [
                'attribute' => 'squeeze',
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data->squeeze) {
                        return Module::t('storage', 'Done');
                    }
                    return Module::t('storage', 'Not done');
                }
            ],
            'upload_ip',
            'updated_at',
            'created_at',
        ],
    ]) ?>

</div>
