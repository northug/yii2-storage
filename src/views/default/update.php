<?php

use yii\helpers\Html;
use northug\storage\Module;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model northug\storage\models\Storage */

$this->title = Module::t('storage', 'Update Storage: ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Module::t('storage', 'Storage'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Module::t('storage', 'Update File');
?>
<div class="storage-update">

    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
            ]); ?>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
