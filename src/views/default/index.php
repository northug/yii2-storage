<?php

use yii\helpers\Html;
use yii\grid\GridView;
use northug\storage\Module;
use yii\widgets\Breadcrumbs;
use northug\storage\assets\MainAsset;
MainAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel northug\storage\models\_search\StorageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categories array */

$this->title = Module::t('storage', 'Storage');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="storage-index">

    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
        ]);
        ?>
    </div>

<?php echo $this->render('_search', ['model' => $searchModel, 'categories' => $categories]);  ?>

    <?php if (\Yii::$app->session->hasFlash('result-compress')): ?>
        <div class="col-xl-12 col-lg-12 p-a-0">
            <div class="alert alert-success alert-fill alert-close alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <?php echo \Yii::$app->session->getFlash('result-compress'); ?>
            </div>
        </div>
    <?php endif; ?>
    
    <p>
        <?= Html::a(Module::t('storage', 'Open filter'), ['#'], [
            'class' => 'btn btn-primary open-filter-in-storage close-filter',
            'data-open' => Module::t('storage', 'Open filter'),
            'data-close' => Module::t('storage', 'Close filter'),
            ]) ?>
    </p>
    <hr>
    
    <div style="display:flow-root">
        <p style="float:left">
            <?= Html::a(Module::t('storage', 'Add file(s)'), ['create'], ['class' => 'btn btn-inline btn-success']) ?>
            <?= Html::a(Module::t('storage-category', 'Categories'), ['/storage/categories'], ['class' => 'btn btn-inline btn-primary']) ?>
            <?= Html::a(Module::t('storage-category', 'Settings'), ['/storage/settings'], ['class' => 'btn btn-inline btn-info']) ?>
        </p>

        <p style="float:right">
            <?= Html::a(Module::t('storage', 'Compression files from storage'), ['/storage/default/compressions', 'limit' => 20], ['class' => 'btn btn-inline btn-secondary-outline']) ?>
            <?= Html::a(Module::t('storage', 'Compress modified files'), [
                '/storage/default/compression-resize-files', 
                'path' => '@frontend/web/uploads',
                'limit' => 20,
                ], ['class' => 'btn btn-inline btn-secondary-outline']) ?>
        </p>
    </div>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'path',
                'label' => 'Файл',
                'format' => 'html',
                'value' => function ($data) {
                    if ($data->type_file == $data::TYPE_FILE_IMAGE) {
                        return Html::img($data->getFullPath(), ['width' => '150']);
                    }
                    return Html::tag('div', '.'.$data->expansion, ['class' => 'default-storage-image']);
                },
            ],
            'alt',
            'old_name',
            'expansion',
            //'width',
            //'height',
            [
                'attribute' => 'size',
                'content' => function ($data) {
                    return Yii::$app->formatter->asShortSize($data->size);
                }
            ],
            [
                'attribute' => 'squeeze',
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data->squeeze) {
                        return Module::t('storage', 'Done');
                    }
                    return Module::t('storage', 'Not done');
                }
            ],
            //'upload_ip',
            //'updated_at',
            'created_at:date',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
