<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $domain string */
/* @var $attribute string */
/* @var $pages yii\data\Pagination */
/* @var $files northug\storage\models\Storage */
//var_dump($attribute);
foreach ($files as $file) {
    $content = '';
    $content .= Html::beginTag('div', ['class' => 'col-md-2 file-item', 'data-id' => $file->id]);

    if ($file->isImage()) {
        $content .= Html::img($domain.$file->path);
    } else {
        $name = explode('.', end(explode('/', $file->path)))[0];
        $startName = $name[0].$name[1].$name[2].$name[3];
        $endName = $name[strlen($name)-2].$name[strlen($name)-1];
        $content .= Html::tag('span', Html::tag('span', $startName.'...'.$endName.'.'.$file->expansion));
    }
    $content .= Html::beginTag('div', ['class' => 'file-buttons col-md-12']);
    $content .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $domain . $file->path, [
                'class' => 'fancybox',
                'data-fancybox' => $attribute
    ]);
    $content .= Html::a('<span class="glyphicon glyphicon-copy"></span>', false, [
                'title' => 'Скопировать путь в буфер обмена',
                'aria-label' => 'Сохранить путь в буфер обмена',
                'id' => 'fileClipboard-save-path' . $file->id,
                'data-clipboard-text' => $domain . $file->path
    ]);
    $content .= Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/storage/default/update', 'id' => $file->id]), [
                'title' => 'Редактировать',
                'class' => 'a-glyphicon-pencil',
                'target' => '_blank',
                'aria-label' => 'Редактировать',
    ]);
    $content .= Html::endTag('div');
    
    $content .= Html::input('checkbox');
    
    $content .= Html::endTag('div');
    $this->registerJs("new Clipboard('#fileClipboard-save-path" . $file->id . "');", View::POS_READY);
    echo $content;
}
echo Html::beginTag('div', ['class' => 'col-md-12 row pagination-block']);
echo LinkPager::widget([
    'pagination' => $pages,
    'linkOptions' => [
        //'data-method' => 'post',
        //'data-pjax' => 1
    ]
]);
echo Html::endTag('div');
