<?php

use yii\helpers\Url;
use yii\web\View;
use yii\helpers\Html;

/* @var $domain string */
/* @var $name string */
/* @var $attribute string */
/* @var $files northug\storage\models\Storage */

foreach ($files as $keyFile => $file) {
    $content = '';

    $is_image = $file->is_image();
    if ($is_image) {
        $class = 'ui-sortable-handle';
        $style = 'background-image: url("' . $domain . $file->path . '");';
    } else {
        $class = 'ui-sortable-handle default-storage-image';
        $style = '';
    }

    $content .= Html::beginTag('li', [
                'style' => $style,
                'class' => $class,
    ]);
    
    if (!$is_image) {
        $content .= '.'.$file->expansion;
    }
    
    $content .= Html::beginTag('div', ['class' => 'file-buttons']);
    $content .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $domain . $file->path, [
                'class' => 'fancybox',
                'data-fancybox' => $attribute
    ]);
    $content .= Html::a('<span class="glyphicon glyphicon-copy"></span>', false, [
                'title' => 'Скопировать путь в буфер обмена',
                'aria-label' => 'Сохранить путь в буфер обмена',
                'id' => 'fileClipboard-save-path' . $file->id,
                'data-clipboard-text' => $domain . $file->path
    ]);
    $content .= Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/storage/default/update', 'id' => $file->id]), [
                'title' => 'Редактировать',
                'class' => 'a-glyphicon-pencil',
                'target' => '_blank',
                'aria-label' => 'Редактировать',
    ]);
    $content .= Html::tag('span', false, [
                'class' => 'glyphicon glyphicon-trash delete-file',
                'data-id' => $file->id,
                'data-post' => $attribute
    ]);
    $content .= Html::endTag('div');
    $content .= Html::input('hidden', $name . '[storage-' . $attribute . '][]', $file->id, ['class' => 'input-id']);
    $content .= Html::endTag('li');

    $this->registerJs("new Clipboard('#fileClipboard-save-path" . $file->id . "');", View::POS_READY);
    echo $content;
}
?>
