<?php

use yii\helpers\Url;
use yii\web\View;
use yii\helpers\Html;
use kartik\sortinput\SortableInput;

/* @var $domain string */
/* @var $name string */
/* @var $attribute string */
/* @var $files northug\storage\models\Storage */

if ($files) {
    $data = [
        'name' => $name . '-' . $attribute,
        'items' => [],
        'hideInput' => true,
        'options' => ['class' => 'form-control', 'readonly' => true, 'id' => $attribute],
        'sortableOptions' => ['type' => 'grid', 'options' => ['class' => 'list-files'],]
    ];
    foreach ($files as $keyFile => $file) {
        $content = '';

        $content .= Html::beginTag('div', ['class' => 'file-buttons']);
        $content .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $domain . $file->path, [
                    'class' => 'fancybox',
                    'data-fancybox' => $attribute
        ]);
        $content .= Html::a('<span class="glyphicon glyphicon-copy"></span>', false, [
                    'title' => 'Скопировать путь в буфер обмена',
                    'aria-label' => 'Сохранить путь в буфер обмена',
                    'id' => 'fileClipboard-save-path' . $file->id,
                    'data-clipboard-text' => $domain . $file->path
        ]);
        $content .= Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/storage/default/update', 'id' => $file->id]), [
                    'title' => 'Редактировать',
                    'class' => 'a-glyphicon-pencil',
                    'target' => '_blank',
                    'aria-label' => 'Редактировать',
        ]);
        $content .= Html::tag('span', false, [
                    'class' => 'glyphicon glyphicon-trash delete-file',
                    'data-id' => $file->id,
                    'data-post' => $attribute
        ]);
        $content .= Html::endTag('div');
        $content .= Html::input('hidden', $nameFilesInStorage, $file->id, ['class' => 'input-id']);

        if ($file->is_image()) {
            $class = 'ui-sortable-handle';
            $style = 'background-image: url("' . $domain . $file->path . '");';
        } else {
            $class = 'ui-sortable-handle default-storage-image';
            $style = '';
        }

        $data['items'][] = [
            'content' => $content,
            'options' => [
                'style' => $style,
                'class' => $class,
            ]
        ];
        $this->registerJs("new Clipboard('#fileClipboard-save-path" . $file->id . "');", View::POS_READY);
    }
    echo SortableInput::widget($data);
}
?>
