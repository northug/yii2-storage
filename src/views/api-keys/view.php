<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use northug\storage\Module;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model northug\storage\models\StorageApiKeys */

$this->title = $model->key;
$this->params['breadcrumbs'][] = ['label' => Module::t('storage', 'Storage'), 'url' => ['/storage/default/index']];
$this->params['breadcrumbs'][] = ['label' => Module::t('storage-api-keys', 'Settings'), 'url' => ['/storage/settings/index']];
$this->params['breadcrumbs'][] = ['label' => Module::t('storage-api-keys', 'Api Keys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storage-api-keys-view">

    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
            ]); ?>
    </div>

    <p>
        <?= Html::a(Module::t('storage-api-keys', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Module::t('storage-api-keys', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'key',
        ],
    ]) ?>

</div>
