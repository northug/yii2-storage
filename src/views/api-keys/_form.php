<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use northug\storage\Module;

/* @var $this yii\web\View */
/* @var $model northug\storage\models\StorageApiKeys */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="storage-api-keys-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Module::t('storage-api-keys', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
