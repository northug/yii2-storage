<?php

use yii\helpers\Html;
use yii\grid\GridView;
use northug\storage\Module;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel northug\storage\models\_search\ApiKeysSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('storage-api-keys', 'Api Keys');
$this->params['breadcrumbs'][] = ['label' => Module::t('storage', 'Storage'), 'url' => ['/storage/default/index']];
$this->params['breadcrumbs'][] = ['label' => Module::t('storage-api-keys', 'Settings'), 'url' => ['/storage/settings/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storage-api-keys-index">

    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
            ]); ?>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Module::t('storage-api-keys', 'Create new key'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'key',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
