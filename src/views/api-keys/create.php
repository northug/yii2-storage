<?php

use yii\helpers\Html;
use northug\storage\Module;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model northug\storage\models\StorageApiKeys */

$this->title = Module::t('storage-api-keys', 'Creating key');
$this->params['breadcrumbs'][] = ['label' => Module::t('storage', 'Storage'), 'url' => ['/storage/default/index']];
$this->params['breadcrumbs'][] = ['label' => Module::t('storage-api-keys', 'Settings'), 'url' => ['/storage/settings/index']];
$this->params['breadcrumbs'][] = ['label' => Module::t('storage-api-keys', 'Api Keys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storage-api-keys-create">

    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
            ]); ?>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
