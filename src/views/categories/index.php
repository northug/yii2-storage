<?php

use yii\helpers\Html;
use yii\grid\GridView;
use northug\storage\Module;
use yii\widgets\Breadcrumbs;
use northug\storage\models\Categories;

/* @var $this yii\web\View */
/* @var $searchModel northug\storage\models\_search\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('storage-category', 'Categories');
$this->params['breadcrumbs'][] = ['label' => Module::t('storage', 'Storage'), 'url' => ['/storage']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
            ]); ?>
    </div>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Module::t('storage-category', 'Create categories'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'status',
                'filter' => [
                    Categories::STATUS_ACTIVE => Module::t('storage-category', 'Active'),
                    Categories::STATUS_INACTIVE => Module::t('storage-category', 'Not active'),
                ],
                'content' => function ($data) {
                    return $data->status ? Module::t('storage-category', 'Active') : Module::t('storage-category', 'Not active');
                }
            ],
            'sort',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
