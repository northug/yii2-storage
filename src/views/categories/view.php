<?php

use yii\helpers\Html;
use northug\storage\Module;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model northug\storage\models\Categories */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('storage-category', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-view">

    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
            ]); ?>
    </div>

    <p>
        <?= Html::a(Module::t('storage-category', 'Update category'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Module::t('storage-category', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'status',
            'sort',
        ],
    ]) ?>

</div>
