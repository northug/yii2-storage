<?php

use yii\helpers\Html;
use northug\storage\Module;
use yii\widgets\Breadcrumbs;


/* @var $this yii\web\View */
/* @var $model northug\storage\models\Categories */

$this->title = Module::t('storage-category', 'Create Categories');
$this->params['breadcrumbs'][] = ['label' => Module::t('storage-category', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-create">

    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
            ]); ?>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
