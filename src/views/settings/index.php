<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use northug\storage\Module;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\modules\storage\models\StorageSettings */
/* @var $form ActiveForm */

$this->title = Module::t('storage-settings', 'Storage settings');
$this->params['breadcrumbs'][] = ['label' => Module::t('storage', 'Storage'), 'url' => ['/storage/default/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="storage-settings">
    
    <div class="section-header">
        <h3><?= Html::encode($this->title) ?></h3>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'breadcrumb breadcrumb-simple'],
            ]); ?>
    </div>
    
    <p>
        <?= Html::a(Module::t('storage-settings', 'List of keys from the site tinypng.com'), ['/storage/api-keys'], [
            'class' => 'btn btn-success',
            ]) ?>
    </p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status_squeeze')->checkbox(); ?>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'storage_sum_files',
            [
                'attribute' => 'storage_sum_size',
                'format' => 'raw',
                'value' => function ($data) {
                    return Yii::$app->formatter->asShortSize($data->storage_sum_size);
                }
            ],
        ],
        'options' => [
            'class' => 'table table-striped table-bordered detail-view',
            'style' => 'margin-bottom:15px'
        ]
    ])
    ?>

    <div class="form-group">
        <?= Html::submitButton(Module::t('storage-settings', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- storage-settings -->
