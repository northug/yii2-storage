<?php

namespace northug\storage\assets;
use yii\web\AssetBundle;

class MainAsset extends AssetBundle
{
    public $sourcePath = '@northug/storage/web';
    public $css = [
        'css/jquery.fancybox.min.css',
        'css/storage.css',
        '/static/css/separate/pages/gallery.min.css',
    ];
    public $js = [
        'js/jquery.fancybox.min.js',
        'js/storage.js',
        'js/clipboard.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}