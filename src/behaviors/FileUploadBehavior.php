<?php

namespace northug\storage\behaviors;

use Yii;
use yii\base\Behavior;
use northug\storage\Module;
use northug\storage\models\unload\MStorage;
use northug\storage\models\Storage;
use northug\storage\models\unload\StorageUnload;
use northug\storage\models\Categories;
use northug\storage\models\FileUpload;
use northug\storage\models\StorageToModel;
use northug\storage\models\FileUploadValidate;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class FileUploadBehavior extends Behavior {

    /**
     * List of all attributes
     * @var array 
     */
    public $attributes = null;

    /**
     * Options
     * Example:
     * 'options' => [
        'img' => [
            'name' => 'filename',
            'dir' => '/resume/doctors',
            'subdirectories' => 5
        ]
    ],
     * @var array
     */
    public $options = null;

    /**
     * Deletes old files from the repository
     * Example [
     *       'img' => true,
     *       'img2' => false,
     * ]
     * @return boolean
     */
    public $deleteOldFilesInStorage = null;

    public function events() {
        if (isset(Yii::$app->request->isPost) and !Yii::$app->request->isPost) {
            return [];
        }
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'uploads',
            ActiveRecord::EVENT_AFTER_UPDATE => 'uploads',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'validates',
        ];
    }

    /**
     * Uploading all files
     * @return boolean
     */
    public function uploads() {
        if ($this->attributes === null or ! is_array($this->attributes) or ! $this->owner->validate() or get_class(Yii::$app) == 'yii\console\Application') {
            return false;
        }
        foreach ($this->attributes as $attribute) {
            if (!$this->upload($attribute)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Uploading files for one attribute
     * @param string $attribute
     * @return boolean
     */
    public function upload($attribute) {

        if (!$this->checkingAttribute($attribute)) {
            return false;
        }

        $this->owner->$attribute = UploadedFile::getInstances($this->owner, $attribute);
        $newFiles = [];

        if ($this->owner->$attribute) {
            foreach ($this->owner->$attribute as $file) {
                $pathFile = (new FileUpload())->upload($file, $this->getOptions($attribute));
                if ($pathFile) {
                    $newFiles[] = (new Storage([
                        'path' => $pathFile,
                        'old_name' => $file->name,
                            ]))->add();
                }
            }
            $this->setCategories($newFiles, $attribute);
        }

        $this->uploadStorageFile($attribute);

        if (method_exists($this->owner, 'tableName')) {
            $this->setRelations($newFiles, $attribute);
        }

        return true;
    }

    private function getOptions($attribute) {
        return isset($this->options[$attribute]) ? $this->options[$attribute] : false;
    }

    /**
     * Adds new files to the vault and removes unnecessary files
     * @param string $attribute
     */
    private function uploadStorageFile($attribute) {
        $storageFiles = Yii::$app->request->post($this->owner->formName())['storage-' . $attribute];
        if ($storageFiles) {
            foreach ($storageFiles as $key => $storageFile) {
                $model = StorageToModel::getOneByAttribute($this->owner, $attribute, $storageFile);
                if (!$model) {
                    $model = new StorageToModel([
                        'model' => $this->owner->formName(),
                        'attribute' => $attribute,
                        'model_id' => $this->owner->id,
                        'file_id' => $storageFile,
                    ]);
                }
                $model->sort = $key;
                $model->save();
            }
            StorageToModel::deleteAll([
                'AND', '`model` = :attribute_1', '`attribute` = :attribute_2', '`model_id` = :attribute_3', [
                    'NOT IN', 'file_id', $storageFiles]
                    ], [
                ':attribute_1' => $this->owner->formName(),
                ':attribute_2' => $attribute,
                ':attribute_3' => $this->owner->id,
            ]);
        } else {
            StorageToModel::deleteAll([
                'model' => $this->owner->formName(),
                'attribute' => $attribute,
                'model_id' => $this->owner->id,
            ]);
        }
    }

    /**
     * Adds or removes links of files added from the repository to models
     * @param Storage $files
     * @param string $attribute
     * @return boolean
     */
    public function setRelations($files, $attribute) {
        $listInsert = [];
        $modelName = $this->owner->formName();
        $modelId = $this->owner->id;

        $countFiles = count(StorageToModel::getFilesByAttribute($this->owner, $attribute));
        if ($countFiles) {
            $sort = $countFiles++;
        } else {
            $sort = 0;
        }

        foreach ($files as $file) {
            if ($file->id) {
                $listInsert[] = [
                    $modelName,
                    $attribute,
                    $modelId,
                    $file->id,
                    $sort,
                ];
            }
        }
        $countInsert = Yii::$app->db->createCommand()->batchInsert(StorageToModel::tableName(), [
                    'model',
                    'attribute',
                    'model_id',
                    'file_id',
                    'sort',
                        ], $listInsert)->execute();
        if ($countInsert) {
            return $countInsert;
        }
        return false;
    }

    /**
     * Adds or deletes file associations with categories
     * @param Storage $files
     * @param string $attribute
     */
    public function setCategories($files, $attribute = false) {
        $post = \Yii::$app->request->post($this->owner->formName());
        if ($attribute and isset($post[$attribute]['categories'])) {
            $categories = $post[$attribute]['categories'];
        } elseif (isset($post['categories'])) {
            $categories = $post['categories'];
        }
        if (!is_array($files)) {
            $files = [$files];
        }
        Categories::addFilesToCategories($files, $categories);
    }

    /**
     * Validation of files
     * @return boolean
     */
    public function validates() {
        if ($this->attributes and isset(Yii::$app->request->isPost) and Yii::$app->request->isPost) {
            foreach ($this->attributes as $attribute) {
                $validator = new FileUploadValidate([
                    'model' => $this->owner,
                    'attribute' => $attribute,
                ]);
                $validator->validate();
                if ($validator->error) {
                    $this->owner->addError($attribute, $validator->error);
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Checking for the existence of an attribute
     * @param string $attribute
     * @return boolean
     */
    private function checkingAttribute($attribute) {
        return property_exists($this->owner, $attribute);
    }

    /**
     * Returns the MStorage object with file information
     * @param string $attribute
     * @param boolean $unloadStatus
     * @return \northug\storage\models\unload\MStorage
     */
    public function getFile($attribute, $unloadStatus = false) {
        if ($this->owner->$attribute !== null and !$unloadStatus) {
            return $this->owner->$attribute;
        }
        $unload = new StorageUnload();
        $file = $unload->getFile($this->owner, $attribute, $unloadStatus);
        $this->owner->$attribute = $file ? $file : false;
        return $this->owner->$attribute;
    }

    /**
     * Returns MStorage objects with file information
     * @param string $attribute
     * @param boolean $unloadStatus
     * @return \northug\storage\models\unload\MStorage
     */
    public function getFiles($attribute, $unloadStatus = false) {
        if ($this->owner->$attribute !== null and !$unloadStatus) {
            return $this->owner->$attribute;
        }
        $unload = new StorageUnload();
        $files = $unload->getFiles($this->owner, $attribute, $unloadStatus);
        $this->owner->$attribute = $files ? $files : false;
        return $this->owner->$attribute;
    }

}
