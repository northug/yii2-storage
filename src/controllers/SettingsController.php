<?php

namespace northug\storage\controllers;

use Yii;
use northug\storage\models\StorageSettings;

class SettingsController extends \yii\web\Controller {

    public function actionIndex() {

        $model = StorageSettings::findOne(1);

        if ($model->load(Yii::$app->request->post()) and $model->save()) {
            return $this->refresh();
        }

        return $this->render('index', [
                    'model' => $model,
        ]);
    }

}
