<?php

namespace northug\storage\controllers;

use Yii;
use yii\data\Pagination;
use northug\storage\models\Storage;
use northug\storage\models\_search\FilterSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Storage model.
 */
class LibraryController extends Controller {

    const LIMIT_LOAD_FILES = 54;

    public $mainDomain = '';

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                //'load' => ['POST'],
                ],
            ],
        ];
    }

    public function actionLoad($page = 1) {
        $searchModel = new FilterSearch();
        $query = $searchModel->search(Yii::$app->request->queryParams);

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => self::LIMIT_LOAD_FILES]);
        $pages->pageSizeParam = false;
        $pages->params = Yii::$app->request->post($searchModel->formName());
        $files = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        return $this->renderAjax('load', [
                    'files' => $files,
                    'pages' => $pages,
                    'attribute' => Yii::$app->request->get('attribute'),
                    'domain' => $this->getMainDomain(),
        ]);
    }

    private function loadFiles() {
        return Storage::find()->orderBy(['id' => SORT_DESC]);
    }

    public function actionAdd() {
        $IDfiles = json_decode(Yii::$app->request->post('files'));
        $name = Yii::$app->request->post('name');
        $attribute = Yii::$app->request->post('attribute');
        $files = Storage::find()->where(['id' => $IDfiles])->all();

        $view = 'add';
        if (Yii::$app->request->post('sortable') != null) {
            $view = 'add-sortable';
        }

        $nameFilesInStorage = $name . '[storage-' . $attribute . '][]';

        return $this->renderAjax($view, [
                    'files' => $files,
                    'name' => $name,
                    'attribute' => $attribute,
                    'domain' => $this->getMainDomain(),
                    'nameFilesInStorage' => $nameFilesInStorage,
        ]);
    }

    private function getMainDomain() {
        $currentHost = Yii::$app->request->headers->get('host');
        $host = 'http://' . substr($currentHost, strpos($currentHost, ".") + 1, strlen($currentHost));
        $homeUrl = Yii::$app->modules['storage'] ? Yii::$app->modules['storage']->homeUrl : null;
        return $homeUrl ? $homeUrl : $host;
    }

    private function giveHost($host_with_subdomain) {
        $array = explode(".", $host_with_subdomain);
        return (array_key_exists(count($array) - 2, $array) ? $array[count($array) - 2] : "") . "." . $array[count($array) - 1];
    }

}
