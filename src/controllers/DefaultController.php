<?php

namespace northug\storage\controllers;

use Yii;
use northug\storage\Module;
use northug\storage\models\Storage;
use northug\storage\models\tinypng\TinyPng;
use northug\storage\models\CompressResizeFile;
use northug\storage\models\Categories;
use northug\storage\models\form\FileForm;
use northug\storage\models\StorageApiKeys;
use northug\storage\models\StorageSettings;
use northug\storage\models\_search\StorageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Storage model.
 */
class DefaultController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Storage models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new StorageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $categories = Categories::getAllSelect();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'categories' => $categories
        ]);
    }

    /**
     * Displays a single Storage model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }
    
    public function actionCompress($id) {
        $model = $this->findModel($id);
        
        if (!$model) {
            return $this->redirect(['index']);
        }
        
        if ($model->compression()) {
            $model->squeeze = 1;
            $model->save();
        }
        
        return $this->redirect(['view', 'id' => $id]);
    }
    
    public function actionCompressions() {
        $models = Storage::find()->notCompressed()->isImage()->limit(Yii::$app->request->get('limit'))->orderBy(['size' => SORT_DESC])->all();
        if (!$models) {
            Yii::$app->session->setFlash('result-compress', Module::t('storage', 'No files to compress'));
            return $this->redirect(['index']);
        }
        
        $keys = StorageApiKeys::getAll();
        if (!$keys) {
            Yii::$app->session->setFlash('result-compress', Module::t('storage', 'No api keys'));
            return $this->redirect(['index']);
        }
        
        $sumCompress = 0;
        $sumSizeCompress = 0;
        foreach ($models as $model) {

            $absolutePath = $model->getAbsolutePath($model->path);
            $oldSize = filesize($absolutePath);
            
            foreach ($keys as $key) {
                $tiny = new TinyPng(['apiKey' => $key->key]);
                if ($tiny->usage() > StorageApiKeys::MAX_FILES_IN_MONTH) {
                    continue;
                }
                
                if ($newSize = $tiny->compress($absolutePath)) {
                    $sumCompress++;
                    $sumSizeCompress += $oldSize - $newSize;
                    $model->size = $newSize;
                    $model->squeeze = 1;
                    $model->save();
                }
                break;
            }
        }
        $settings = StorageSettings::findOne(1);
        $settings->storage_sum_files += $sumCompress;
        $settings->storage_sum_size += $sumSizeCompress;
        $settings->save();
        
        Yii::$app->session->setFlash('result-compress', Module::t('storage', 'Compressed files: {sum}', ['sum' => $sumCompress]));
        return $this->redirect(['index']);
    }
    
    public function actionCompressionResizeFiles() {
        $path = Yii::$app->request->get('path');
        $compress = new CompressResizeFile([
            'path' => $path,
        ]);
        $compress->setFiles();
        $countResizeFiles = $compress->compress();

        Yii::$app->session->setFlash('result-compress', Module::t('storage', 'Compressed files: {sum}', ['sum' => $countResizeFiles]));
        return $this->redirect(['index']);
    }

    public function actionCreate() {
        $model = new FileForm();

        if (Yii::$app->request->isPost and $model->uploads()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Storage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save(true, ['alt'])) {
            $model->setCategories($model);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Storage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Storage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Storage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Storage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
