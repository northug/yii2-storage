<?php

namespace northug\storage\models\query;

use northug\storage\models\Storage;

/**
 * This is the ActiveQuery class for [[\northug\storage\models\Storage]].
 *
 * @see \common\models\Bid
 */
class StorageQuery extends \yii\db\ActiveQuery
{

    public function notCompressed() {
        return $this->andWhere(['squeeze' => 0]);
    }
    
    public function isImage() {
        return $this->andWhere(['type_file' => Storage::TYPE_FILE_IMAGE]);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Bid[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Bid|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
