<?php

namespace northug\storage\models;

use Yii;
use northug\storage\Module;
use northug\storage\models\Storage;
use northug\storage\models\StorageToCategory;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "storage_category".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property int $sort
 */
class Categories extends \yii\db\ActiveRecord {

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'storage_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['status', 'sort'], 'integer'],
            [['status'], 'default', 'value' => 1],
            [['sort'], 'default', 'value' => 0],
            [['name'], 'string', 'max' => 255],
        ];
    }
    
    /**
     * 
     * @return Categories
     */
    public static function getAllSelect()
    {
        return static::find()
                ->select('name')
                ->where(['status' => self::STATUS_ACTIVE])
                ->orderBy(['sort' => SORT_DESC])
                ->indexBy('name')
                ->column();
    }
    
    /**
     * 
     * @param \northug\storage\models\Storage $files
     * @param array $categories
     */
    public static function addFilesToCategories($files, $categories)
    {
        if ($files and is_array($files) and !$categories) {
            static::removeRelations($files, $categories);
            return true;
        }
        if (!$files or !$categories or !is_array($files) or !is_array($categories)) {
            return false;
        }
        foreach ($categories as $categoryName) {
            $category = static::getOneByName($categoryName);
            if (!$category) {
                $category = new static(['name' => $categoryName]);
                $category->save();
            }
            foreach ($files as $file) {
                if ($file instanceof Storage) {
                    $StorageToCategory = StorageToCategory::getOne($file->id, $category->id);
                    if (!$StorageToCategory) {
                        $StorageToCategory = new StorageToCategory([
                            'storage_id' => $file->id,
                            'category_id' => $category->id
                        ]);
                        $StorageToCategory->save();
                    }
                }
            }
        }
        static::removeRelations($files, $categories);
        return true;
    }
    
    /**
     * Removes unnecessary file associations with categories
     * @param \northug\storage\models\Storage $files
     * @param array $categories
     */
    private static function removeRelations($files, $categories)
    {
        if ($categories) {
            $deleteCategoriesIDs = static::find()->select('id')->where(['not in', 'name', $categories])->column();
            foreach ($files as $file) {
                StorageToCategory::deleteAll([
                    'category_id' => $deleteCategoriesIDs,
                    'storage_id' => $file->id,
                ]);
            }
        }
    }
    
    /**
     * 
     * @param string $name
     * @return Categories|null
     */
    public static function getOneByName($name)
    {
        return static::find()->where(['name' => $name])->one();
    }

    /**
     * 
     * @param array $IDs
     * @return Categories
     */
    public static function getCategoryByIDsFindArray($IDs)
    {
        return static::find()->where(['id' => $IDs])->asArray()->all();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => Module::t('storage-category', 'Name category'),
            'status' => Module::t('storage-category', 'Status'),
            'sort' => Module::t('storage-category', 'Sort'),
        ];
    }

}
