<?php

namespace northug\storage\models;

use Yii;
use yii\base\Model;
use claviska\SimpleImage;
use yii\helpers\FileHelper;
use northug\storage\models\Storage;

/**
 * Description of FileModification
 * @var claviska\SimpleImage $simpleImage
 * @var northug\storage\models\unload\MStorage $model
 * @author Пользователь
 */
class FileModification extends Model {

    /**
     * Class object
     * @var \northug\storage\models\unload\MStorage 
     */
    public $model;

    /**
     * Relative path to the folder with compressed files
     * @var string 
     */
    public $dirResize = '/uploads/resize';

    /**
     * Maximum memory capacity bar for php. Default 100 Mb
     * @var integer 
     */
    public $memoryLimit = 100 * 1024 * 1024;

    public function __construct($config = []) {
        $this->model = $config;
    }

    /**
     * Returns an object by changing images strictly by size. Old function.
     * @param integer $width
     * @param integer $height
     * @param string $mimeType
     * @param integer $quality
     * @return \northug\storage\models\unload\MStorage
     */
    public function getResizeFile($width = null, $height = null, $mimeType = null, $quality = 100) {
        return $this->resize([
            'width' => $width,
            'height' => $height,
            'mimeType' => $mimeType,
            'quality' => $quality,
        ]);
    }
    
    /**
     * $options = [
     *      'width' => 100,
     *      'height' => 100,
     *      'mimeType' => null,
     *      'quality' => 100,
     *      'maxWidth' => 100,
     *      'maxHeight' => 100,
     *      'minHeight' => 100,
     *      'minHeight' => 100,
     * ]
     * @param array $options
     * @param \northug\storage\models\unload\MStorage $options
     */
    public function resize($options = []) {
        $this->defaultResizeOptions($options);
        $this->sizing($options);
        if ($this->model->type_file == Storage::TYPE_FILE_IMAGE and $this->getMemoryLimit()) {
            $this->newNameFile($options['width'], $options['height'], $options['mimeType'], 'resize-' . $options['quality']);
            $resizePath = $this->getResizePath();
            $absolutePath = Yii::getAlias($this->model->absolutePath . $resizePath);
            if (!$this->checkFile($absolutePath)) {
                $this->createPath($absolutePath);
                try {
                    if ($image = $this->getSimpleImage()) {
                        $image->resize($options['width'], $options['height'])->toFile($absolutePath, $options['mimeType'], $options['quality']);
                        $this->model->path = $resizePath;
                    }
                } catch (\Exception $err) {
                    $this->model->path = $this->model->originalPath;
                }
            } else {
                $this->model->path = $resizePath;
            }
        }

        return $this->model;
    }
    
    /**
     * Returns an object by changing the image by adjusting the size without disturbing the aspect ratio
     * @param integer $width
     * @param integer $height
     * @param string $anchor
     * @param string $mimeType
     * @param integer $quality
     * @return \northug\storage\models\unload\MStorage
     */
    public function getThumbnailFile($width = null, $height = null, $anchor = 'center', $mimeType = null, $quality = 100) {
        return $this->thumb([
            'width' => $width,
            'height' => $height,
            'anchor' => $anchor,
            'mimeType' => $mimeType,
            'quality' => $quality,
        ]);
    }
    
    /**
     * $options = [
     *      'width' => 100,
     *      'height' => 100,
     *      'mimeType' => null,
     *      'quality' => 100,
     *      'maxWidth' => 100,
     *      'maxHeight' => 100,
     *      'minHeight' => 100,
     *      'minHeight' => 100,
     *      'anchor' => 'center',
     * ]
     * @param array $options
     * @return \northug\storage\models\unload\MStorage
     */
    public function thumb($options = []) {
        $this->defaultThumbnailOptions($options);
        $this->sizing($options);
        if ($this->model->type_file == Storage::TYPE_FILE_IMAGE and $this->getMemoryLimit()) {
            $this->newNameFile($options['width'], $options['height'], $options['mimeType'], 'thumb_' . $options['anchor'] . '-' . $options['quality']);
            $resizePath = $this->getResizePath();
            $absolutePath = Yii::getAlias($this->model->absolutePath . $resizePath);
            if (!$this->checkFile($absolutePath)) {
                $this->createPath($absolutePath);
                try {
                    if ($image = $this->getSimpleImage()) {
                        $image->thumbnail($options['width'], $options['height'], $options['anchor'])->toFile($absolutePath, $options['mimeType'], $options['quality']);
                        $this->model->path = $resizePath;
                    }
                } catch (\Exception $err) {
                    $this->model->path = $this->model->originalPath;
                }
            } else {
                $this->model->path = $resizePath;
            }
        }
        return $this->model;
    }
    
    /**
     * 
     * @param array $options
     */
    private function sizing(&$options) {
        if (!$this->model->width or !$this->model->height) {
            if (file_exists(Yii::getAlias($this->model->absolutePath.$this->model->path))) {
                list($this->model->width, $this->model->height) = getimagesize(Yii::getAlias($this->model->absolutePath.$this->model->path));
            }
        }
        if (isset($options['maxWidth']) and $options['maxWidth'] < $this->model->width) {
            $options['width'] = $options['maxWidth'];
        }
        
        if (isset($options['maxHeight']) and $options['maxHeight'] < $this->model->height) {
            $options['height'] = $options['maxHeight'];
        }
        
        if (isset($options['minWidth']) and $options['minWidth'] > $this->model->width) {
            $options['width'] = $options['minWidth'];
        }
        
        if (isset($options['minHeight']) and $options['minHeight'] > $this->model->height) {
            $options['height'] = $options['minHeight'];
        }
    }
    
    /**
     * 
     * @param array $options
     */
    private function defaultResizeOptions(&$options) {
        if (!isset($options['width'])) $options['width'] = null;
        if (!isset($options['height'])) $options['height'] = null;
        if (!isset($options['mimeType'])) $options['mimeType'] = null;
        if (!isset($options['quality'])) $options['quality'] = 100;
    }
    
    /**
     * 
     * @param array $options
     */
    private function defaultThumbnailOptions(&$options) {
        if (!isset($options['width'])) $options['width'] = null;
        if (!isset($options['height'])) $options['height'] = null;
        if (!isset($options['mimeType'])) $options['mimeType'] = null;
        if (!isset($options['quality'])) $options['quality'] = 100;
        if (!isset($options['anchor'])) $options['anchor'] = 'center';
    }

    /**
     * 
     * @return boolean
     */
    private function getMemoryLimit() {
        return memory_get_usage() < $this->memoryLimit;
    }

    /**
     * 
     * @return SimpleImage
     */
    public function getSimpleImage() {
        $pathToFile = Yii::getAlias($this->model->absolutePath . str_replace('%20', ' ', $this->model->originalPath));
        if (file_exists($pathToFile)) {
            return new SimpleImage($pathToFile);
        }
        return null;
    }

    /**
     * Checks for presence of a file on the server
     * @param string $absolutePath
     * @return boolean
     */
    public function checkFile($absolutePath) {
        return file_exists($absolutePath);
    }

    /**
     * Generates a new file name
     * @param integer $width
     * @param integer $height
     * @param string $strEnd
     */
    public function newNameFile($width, $height, $mimeType = null, $strEnd = '') {
        $nameArray = explode('.', $this->model->originalPath);
        $nameFile = $nameArray[0];
        $expFile = $mimeType === null ? $nameArray[1] : explode('/', $mimeType)[1];
        $str = '_';
        if ($width) {
            $str .= $width;
        } else {
            $str .= '_';
        }
        $str .= 'x';
        if ($height) {
            $str .= $height;
        } else {
            $str .= '_';
        }
        $str .= '_' . $strEnd;
        $this->model->path = $nameFile . $str . '.' . $expFile;
    }

    /**
     * Returns the path for the modified file
     * @var string $path
     * @return string
     */
    public function getResizePath() {
        if ($this->is_in_str($this->model->path, $this->dirResize)) {
            return str_replace('/uploads' . $this->dirResize, $this->dirResize, str_replace('%20', ' ', $this->model->path));
        }
        return str_replace('/uploads', $this->dirResize, str_replace('%20', ' ', $this->model->path));
    }

    /**
     * Checks for the presence of a substring in a string
     * @param string $str
     * @param string $substr
     * @return boolean
     */
    public function is_in_str($str, $substr) {
        $result = strpos($str, $substr);
        if ($result === FALSE) {
            return false;
        }
        return true;
    }

    /**
     * Creates the necessary directories for the new file
     * @param type $pathToFile
     * @return boolean
     */
    public function createPath($pathToFile) {
        $path = Yii::getAlias(dirname($pathToFile));
        return FileHelper::createDirectory(FileHelper::normalizePath($path));
    }

}
