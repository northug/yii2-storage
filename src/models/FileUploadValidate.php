<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace northug\storage\models;

use Yii;
use yii\base\BaseObject;
use yii\web\UploadedFile;
use northug\storage\Module;
use northug\storage\models\Storage;

/**
 * Description of FileUploadValidate
 * @var $this->storageFiles \northug\storage\models\Storage
 * @author Пользователь
 */
class FileUploadValidate extends BaseObject {

    public $model;
    public $attribute;
    public $error;
    public $storageFiles;
    public $storageFilesId;
    public $newFiles;
    public $rules = [];

    public function init() {
        $this->storageFilesId = Yii::$app->request->post($this->model->formName())['storage-' . $this->attribute];
        $this->newFiles = UploadedFile::getInstances($this->model, $this->attribute);
        $this->getFiles();
        $this->getRules();
    }

    private function getFiles() {
        $this->storageFiles = Storage::find()
                ->where(['id' => $this->storageFilesId])
                ->indexBy('id')
                ->all();
    }

    public function validate() {

        foreach ($this->rules as $rule) {

            if (!$this->validateCountFiles($rule['maxFiles'])) {
                $this->addError('Upload limit exceeded');
                return false;
            }

            if (!$this->validateExtensions($rule['extensions'])) {
                $this->addError('You can only upload files of such extensions: {extensions}', ['extensions' => $rule['extensions']]);
                return false;
            }
            
            if (!$this->validateProfile($rule['maxWidth'], $rule['maxHeight'], $rule['minWidth'], $rule['minHeight'])) {
                return false;
            }
            
            if (!$this->validateSize($rule['maxSize'], $rule['minSize'])) {
                return false;
            }
            
        }
        return true;
    }
    
    private function validateSize($maxSize, $minSize) {
        foreach ($this->storageFiles as $file) {
            if ($maxSize and $maxSize < $file->size) {
                $this->addError('Maximum image size {num}', ['num' => Yii::$app->formatter->asShortSize($maxSize)]);
                return false;
            }
            if ($minSize and $minSize > $file->size) {
                $this->addError('Minimum image size {num}', ['num' => Yii::$app->formatter->asShortSize($minSize)]);
                return false;
            }
        }
        return true;
    }

    private function validateProfile($maxWidth, $maxHeight, $minWidth, $minHeight) {
        foreach ($this->storageFiles as $file) {
            if ($maxWidth and $maxWidth < $file->width) {
                $this->addError('Maximum image width {num}px', ['num' => $maxWidth]);
                return false;
            }
            if ($maxHeight and $maxHeight < $file->height) {
                $this->addError('Maximum image height {num}px', ['num' => $maxHeight]);
                return false;
            }
            if ($minWidth and $minWidth > $file->width) {
                $this->addError('Minimum image width {num}px', ['num' => $minWidth]);
                return false;
            }
            if ($minHeight and $minHeight > $file->height) {
                $this->addError('Minimum image height {num}px', ['num' => $minHeight]);
                return false;
            }
        }
        return true;
    }

    private function validateCountFiles($maxFiles) {
        if ($maxFiles) {
            return $maxFiles >= (count($this->newFiles) + count($this->storageFiles));
        }
        return true;
    }

    private function validateExtensions($extensions) {
        if (!$extensions) {
            return true;
        }
        $extensionsArray = explode(',', $extensions);
        foreach ($this->storageFiles as $file) {
            $flag = false;
            foreach ($extensionsArray as $extension) {
                $extension = trim($extension);
                if ($file->expansion == $extension) {
                    $flag = true;
                }
            }
            if (!$flag) {
                return false;
            }
        }
        return true;
    }

    private function addError($error, $options = []) {
        $this->error = Module::t('storage', $error, $options);
    }

    private function getRules() {
        foreach ($this->model->rules() as $rule) {
            if ($rule[1] == 'file' or $rule[1] == 'image') {
                if (is_array($rule[0]) and in_array($this->attribute, $rule[0])) {
                    $this->rules[] = $rule;
                }
                if (is_string($rule[0])) {
                    $rule[0] = trim($rule[0]);
                    if ($this->attribute == $rule[0]) {
                        $this->rules[] = $rule;
                    }
                }
            }
        }
    }

}
