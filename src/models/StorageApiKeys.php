<?php

namespace northug\storage\models;

use Yii;
use northug\storage\Module;
use northug\storage\models\Storage;
use northug\storage\models\StorageSettings;

/**
 * This is the model class for table "storage_api_key".
 *
 * @property int $id
 * @property string $key
 */
class StorageApiKeys extends \yii\db\ActiveRecord
{
    
    const MAX_FILES_IN_MONTH = 495;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'storage_api_keys';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['key', 'required'],
            ['key', 'string', 'max' => 255],
        ];
    }
    
    public static function getAll() {
        return static::find()->all();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('storage-settings', 'ID'),
            'key' => Module::t('storage-settings', 'Api Key Tiny'),
        ];
    }
}
