<?php

namespace northug\storage\models;

use Yii;
use yii\helpers\FileHelper;
use northug\storage\Module;
use yii\helpers\ArrayHelper;
use northug\storage\models\Categories;
use northug\storage\models\StorageSettings;
use northug\storage\models\StorageApiKeys;
use northug\storage\models\tinypng\TinyPng;
use northug\storage\models\StorageToCategory;
use yii\behaviors\TimestampBehavior;
use yii\web\HttpException;
use northug\storage\behaviors\FileUploadBehavior;

/**
 * This is the model class for table "storage".
 *
 * @property int $id
 * @property string $hash
 * @property string $path
 * @property string $absolutePath
 * @property string $alt
 * @property string $old_name
 * @property string $type_file
 * @property string $expansion
 * @property int $width
 * @property int $height
 * @property int $size
 * @property int $squeeze
 * @property string $upload_ip
 * @property int $updated_at
 * @property int $created_at
 * @property array $categories
 */
class Storage extends \yii\db\ActiveRecord {

    public $categories;
    public $insertCategory = true;
    public $default_absolute_path = '@frontend/web';
    public $replaceOldFile = false;
    
    const TYPE_FILE_IMAGE = 'image';
    const TYPE_FILE_OTHER = 'other-file';

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'storage';
    }

    public function behaviors() {
        return [
            'timestamp' => ['class' => TimestampBehavior::className()],
            FileUploadBehavior::className(),
        ];
    }

    public function init() {
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'deleteFile']);
        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'changeTypeFile']);
        $this->on(self::EVENT_BEFORE_VALIDATE, [$this, 'createHash']);
    }

    /**
     * Deletes a file on disk when it is removed from the database
     * @return boolean
     */
    public function deleteFile() {
        if (file_exists(Yii::getAlias($this->absolutePath . $this->path))) {
            return unlink(Yii::getAlias($this->absolutePath . $this->path));
        }
        return false;
    }
    
    /**
     * Specifies the file type before inserting into the database
     */
    public function changeTypeFile()
    {
        if ($this->is_image()) {
            $this->type_file = self::TYPE_FILE_IMAGE;
        } else {
            $this->type_file = self::TYPE_FILE_OTHER;
        }
    }
    
    /**
     * Create hash absolute path to file
     */
    public function createHash() {
        if ($this->absolutePath === null) {
            $this->absolutePath = $this->default_absolute_path;
        }
        $this->hash = md5($this->absolutePath.$this->path);
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['path'], 'required'],
            [['path'], 'unique', 'message' => Module::t('storage', 'A file with this path already exists')],
            [['hash'], 'unique'],
            [['width', 'height', 'size', 'squeeze', 'updated_at', 'created_at'], 'integer'],
            [['path', 'absolutePath', 'alt', 'old_name', 'type_file', 'hash'], 'string', 'max' => 255],
            [['expansion'], 'string', 'max' => 6],
            [['upload_ip'], 'string', 'max' => 15],
            ['expansion', 'filter', 'filter' => function ($value) {
                    return end(explode('.', $this->path));
                }],
            ['upload_ip', 'filter', 'filter' => function ($value) {
                    if (isset(Yii::$app->request->userIP)) {
                        return Yii::$app->request->userIP;
                    }
                    return null;
                }],
            [['squeeze'], 'default', 'value' => 0],
            [['absolutePath'], 'default', 'value' => $this->default_absolute_path],
        ];
    }

    /**
     * Adding a new file to the repository
     * @return boolean|$this
     */
    public function add() {
        $this->checkingPath();

        $absolutePath = $this->getAbsolutePath($this->path);
        if (!static::checkingFile($absolutePath)) {
            return false;
        }
        $this->getSizes($absolutePath);
        $settings = StorageSettings::findOne(1);
        if ($settings->status_squeeze and $this->compression(Yii::getAlias($absolutePath))) {
            $this->squeeze = 1;
        }
        if ($this->save()) {
            return $this;
        }
        Yii::$app->session->setFlash('errorUploadFiles', $this->getErrors('path')[0]);
        return false;
    }
    
    /**
     * Checks if there is a file with the same path
     */
    public function checkOldFile() {
        if ($this->replaceOldFile) {
            $model = static::find()->where(['path' => $this->path])->one();
            if ($model) {
                return $model;
            }
        }
        return false;
    }

    /**
     * Compresses the image along the specified path and resizes the current object
     * @param string $absolutePath
     * @return boolean
     */
    public function compression($absolutePath = null, $keys = null) {
        $settings = StorageSettings::findOne(1);
        if (!$this->is_image()) {
            return false;
        }
        
        if ($keys === null) {
            $keys = StorageApiKeys::getAll();
        }
        
        if (!$keys) {
            return false;
        }
        
        if ($absolutePath === null) {
            $absolutePath = $this->getAbsolutePath($this->path);
        }
        
        foreach ($keys as $key) {
            $oldSize = filesize($absolutePath);
            $tiny = new TinyPng(['apiKey' => $key->key]);
            if ($tiny->usage() > 500) {
                continue;
            }
            if ($newSize = $tiny->compress($absolutePath)) {
                $settings->storage_sum_files++;
                $settings->storage_sum_size += $oldSize - $newSize;
                $settings->save();
                $this->size = $newSize;
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the width, height, and file size by its absolute path
     * @param string $absolutePath
     */
    public function getSizes($absolutePath) {
        list($this->width, $this->height) = getimagesize($absolutePath);
        $this->size = filesize($absolutePath);
    }

    /**
     * Returns the absolute absolute path
     * @param string $path
     * @return string
     */
    public function getAbsolutePath($path) {
        return Yii::getAlias($this->default_absolute_path . $path);
    }

    /**
     * Checks the file for existence
     * @param string $absolutePath
     * @return boolean
     */
    public static function checkingFile($absolutePath) {
        return file_exists($absolutePath);
    }

    /**
     * Error output when there is no file path in the object
     * @throws HttpException
     */
    public function checkingPath() {
        if ($this->path === null) {
            throw new HttpException(500, Module::t('storage', 'You must specify a file path'));
        }
    }

    /**
     * 
     * @return array
     */
    public function getCategoriesSelect() {
        return Categories::getAllSelect();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'path' => Module::t('storage', 'Path'),
            'absolutePath' => Module::t('storage', 'Absolute path to the root of the site'),
            'old_name' => Module::t('storage', 'Old Name'),
            'alt' => Module::t('storage', 'Alt'),
            'expansion' => Module::t('storage', 'Expansion'),
            'width' => Module::t('storage', 'Width'),
            'height' => Module::t('storage', 'Height'),
            'size' => Module::t('storage', 'Size'),
            'squeeze' => Module::t('storage', 'Squeeze'),
            'upload_ip' => Module::t('storage', 'Upload Ip'),
            'updated_at' => Module::t('storage', 'Updated At'),
            'created_at' => Module::t('storage', 'Created At'),
        ];
    }

    public function getStorageToCategoryRelations() {
        return $this->hasMany(StorageToCategory::className(), ['storage_id' => 'id']);
    }

    public function getCategoriesList() {
        return $this->hasMany(Categories::className(), ['id' => 'category_id'])->via('storageToCategoryRelations')->all();
    }

    public function getCategoriesListSelect() {
        $data = $this->categoriesList;
        $data = ArrayHelper::toArray($data, [
                    'northug\storage\models\Categories' => [
                        'name',
                    ],
        ]);
        return ArrayHelper::getColumn($data, 'name');
    }

    /**
     * Check if the file is an image
     * @return boolean
     */
    public function is_image() {
        if (!$this->absolutePath) {
            $absolutePath = Yii::getAlias($this->default_absolute_path.$this->path);
        } else {
            $absolutePath = Yii::getAlias($this->absolutePath.$this->path);
        }
        $is = @getimagesize($absolutePath);
        if (!$is) {
            return false;
        }
        elseif (!in_array($is[2], array(1, 2, 3))) {
            return false;
        }
        return true;
    }
    
    public function isImage() {
        return $this->type_file == self::TYPE_FILE_IMAGE;
    }
    
    public function getFullPath() {
        $currentHost = Yii::$app->request->headers->get('host');
        $host = substr($currentHost, strpos($currentHost, ".") + 1, strlen($currentHost));
        $homeUrl = Yii::$app->modules['storage'] ? Yii::$app->modules['storage']->homeUrl : null;
        return $homeUrl ? $homeUrl . $this->path : 'http://' . $host . $this->path;
    }

    /**
     * {@inheritdoc}
     * @return \northug\storage\models\query\StorageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \northug\storage\models\query\StorageQuery(get_called_class());
    }
    
}
