<?php

namespace northug\storage\models;

use Yii;
use yii\helpers\ArrayHelper;
use northug\storage\Module;
use northug\storage\models\Storage;

/**
 * This is the model class for table "storage_to_model".
 *
 * @property int $id
 * @property string $model
 * @property string $attribute
 * @property int $model_id
 * @property int $file_id
 * @property int $sort
 */
class StorageToModel extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'storage_to_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['model_id', 'file_id', 'sort'], 'integer'],
            [['sort'], 'default', 'value' => 0],
            [['model', 'attribute'], 'string', 'max' => 255],
        ];
    }

    public static function getFilesByAttribute($model, $attribute) {
        return ArrayHelper::getColumn(static::getAllRelations($model, $attribute), 'file');
    }
    
    public static function getOneByAttribute($model, $attribute, $fileId) {
        return static::find()
                        ->where([
                            'model' => $model->formName(),
                            'attribute' => $attribute,
                            'model_id' => $model->id,
                            'file_id' => $fileId,
                        ])
                        ->one();
    }

    public static function getAllRelations($model, $attribute) {
        return static::find()
                        ->where([
                            'model' => $model->formName(),
                            'attribute' => $attribute,
                            'model_id' => $model->id,
                        ])
                        ->with(['file'])
                        ->orderBy(['sort' => SORT_ASC])
                        ->all();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Module::t('storage-to-model', 'ID'),
            'model' => Module::t('storage-to-model', 'Model'),
            'attribute' => Module::t('storage-to-model', 'Attribute'),
            'model_id' => Module::t('storage-to-model', 'Model ID'),
            'file_id' => Module::t('storage-to-model', 'File ID'),
            'sort' => Module::t('storage-to-model', 'Sort'),
        ];
    }

    public function getFile() {
        return $this->hasOne(Storage::className(), ['id' => 'file_id']);
    }

}
