<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace northug\storage\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * Description of FileUpload
 *
 * @author Пользователь
 */
class FileUpload {

    public $pathUrl = '/uploads';
    public $pathAbsolute = '@frontend/web';

    /**
     * 
     * @param UploadedFile $file
     * @return boolean|string
     */
    public function upload(UploadedFile $file, $options = false) {
        $pathToFile = $this->generatePath($file, $options);
        $absolutePath = $this->pathAbsolute . $pathToFile;
        $this->createPath($absolutePath);
        if ($file->saveAs(Yii::getAlias($absolutePath))) {
            return $pathToFile;
        }
        return false;
    }
    
    private function generatePath($file, $options) {
        if (!$options) {
            $filename = $this->createNameFile($file->tempName);
            return $this->pathUrl . $this->addSubdirectiories($filename) . '.' . $file->extension;
        }
        $subdirectories = isset($options['subdirectories']) ? $options['subdirectories'] : 2;
        $filename = isset($options['name']) ? $this->addSubdirectiories($options['name'], $subdirectories) : $this->addSubdirectiories($this->createNameFile($file->tempName), $subdirectories);
        $dir = isset($options['dir']) ? $options['dir'] : '';
        return $this->pathUrl.$dir.$filename.'.'.$file->extension;
        
    }

    private function addSubdirectiories($filename, $subdirectories = 2) {
        if ($subdirectories) {
            $dir = '/';
            for ($i = 0 ; $i < $subdirectories ; $i++) {
                $dir .= $this->generateStr(2).'/';
            }
            $filename = $dir.$filename;
        } else {
            $filename = '/'.$filename;
        }
        return $filename;
    }

    private function addDirectories($filename) {
        return '/'.static::generateStr(2).'/'.static::generateStr(2).'/'.$filename;
    }

    private function createNameFile($filename) {
        return sha1_file($filename);
    }

    private function createPath($pathToFile) {
        $path = Yii::getAlias(dirname($pathToFile));
        return FileHelper::createDirectory($path);
    }
    
    private static function generateStr($length) {
        $arr = ['a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',];
        $pass = '';
        for ($i = 0; $i < $length; $i++) {
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }
        return $pass;
    }

}
