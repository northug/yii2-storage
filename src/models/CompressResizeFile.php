<?php

namespace northug\storage\models;

use Yii;
use northug\storage\Module;
use yii\helpers\FileHelper;
use northug\storage\models\Storage;
use northug\storage\models\StorageApiKeys;
use northug\storage\models\StorageSettings;
use northug\storage\models\tinypng\TinyPng;
use northug\storage\models\StorageResizeFiles;

/**
 * Description of CompressResizeFile
 *
 * @author Пользователь
 */
class CompressResizeFile {
    
    private $path;
    
    public function __construct($config = []) {
        if (isset($config['path'])) {
            $this->path = $config['path'];
        }
    }
    
    /**
     * 
     * @return boolean|int
     */
    public function compress($limit = 25) {
        $keys = StorageApiKeys::getAll();
        if (!$keys) {
            Yii::$app->session->setFlash('result-compress', Module::t('storage', 'No api keys'));
            return false;
        }
        $files = $this->getFilesNotCompress()->limit($limit)->all();
        $sumCompress = 0;
        $sumSizeCompress = 0;

        foreach ($files as $file) {
            
            $absolutePath = Yii::getAlias($file->absolutePath);
            if (!$this->is_image($absolutePath)) {
                continue;
            }

            $oldSize = filesize($absolutePath);
            
            foreach ($keys as $key) {
                $tiny = new TinyPng(['apiKey' => $key->key]);
                if ($tiny->usage() > StorageApiKeys::MAX_FILES_IN_MONTH) {
                    continue;
                }
                
                if ($newSize = $tiny->compress($absolutePath)) {
                    $sumCompress++;
                    $sumSizeCompress += $oldSize - $newSize;
                    $file->squeeze = 1;
                    $file->save();
                }
                break;
            }
        }
        $settings = StorageSettings::findOne(1);
        $settings->storage_sum_files += $sumCompress;
        $settings->storage_sum_size += $sumSizeCompress;
        $settings->save();
        return $sumCompress;
    }
    
    public function getFilesNotCompress() {
        return StorageResizeFiles::find()->where(['squeeze' => StorageResizeFiles::NOT_COMPRESS]);
    }
    
    public function getFilesCompress() {
        return StorageResizeFiles::find()->where(['squeeze' => StorageResizeFiles::SUCCESS_COMPRESS]);
    }
    
    public function getStorageFiles() {
        $files = Storage::find()->select('id,absolutePath,path,hash')->asArray()->indexBy('hash')->all();
        foreach ($files as &$file) {
            $file = $file['absolutePath'].$file['path'];
        }
        return $files;
    }
    
    public function setFiles() {
        $storageFiles = $this->getStorageFiles();
        $filesInBase = StorageResizeFiles::find()->select('absolutePath')->indexBy('hash')->column();
        $searchFiles = $this->searchFiles($this->path);

        if (!$searchFiles) {
            return 0;
        }

        $files = array_diff($this->getFilesWithHash($searchFiles), array_merge($storageFiles, $filesInBase));

        $insert = [];
        foreach ($files as $pathToFile) {
            $insert[] = [
                $pathToFile,
                md5($pathToFile),
                0,
            ];
        }
        return $this->setInStorageResizeFiles($insert);
    }
    
    public function searchFiles() {
        $absolutePath = static::getAbsolutePath($this->path);
        $deletePath = FileHelper::normalizePath(Yii::getAlias($this->path));
        if (!file_exists($absolutePath)) {
            return null;
        }
        $files = FileHelper::findFiles($absolutePath);

        foreach ($files as &$file) {
            $file = str_replace($deletePath, $this->path, $file);
            $file = str_replace('\\', '/', $file);
        }
        return $files;
    }
    
    private function getFilesWithHash($files) {
        $filesWithHash = [];
        foreach ($files as &$file) {
//            var_dump('@frontend/web'.'/uploads/al/hr/faeacd9ed0ca14e4c66c7100beb0f0e01587416a.jpg');
//            var_dump(md5('@frontend/web'.'/uploads/al/hr/faeacd9ed0ca14e4c66c7100beb0f0e01587416a.jpg'));
            $filesWithHash[md5($file)] = $file;
//            print_r($filesWithHash);exit;
        }
        return $filesWithHash;
    }
    
    private function getAbsolutePath($path) {
        return FileHelper::normalizePath(Yii::getAlias($path));
    }
    
    private function setInStorageResizeFiles($insert) {
        return Yii::$app->db->createCommand()->batchInsert('storage_resize_files', ['absolutePath', 'hash', 'squeeze'], $insert)->execute();
    }
    
    private function is_image($absolutePath) {
        $is = @getimagesize($absolutePath);
        if (!$is) {
            return false;
        }
        elseif (!in_array($is[2], array(1, 2, 3))) {
            return false;
        }
        return true;
    }
}
