<?php

namespace northug\storage\models;

use Yii;
use northug\storage\Module;

/**
 * This is the model class for table "storage_settings".
 *
 * @property int $id
 * @property string $api_key_tiny
 * @property int $storage_sum_files
 * @property string $storage_sum_size
 * @property int $status_squeeze
 */
class StorageSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'storage_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['storage_sum_files', 'status_squeeze'], 'integer'],
            [['storage_sum_size'], 'number'],
            ['status_squeeze', 'default', 'value' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('storage-settings', 'ID'),
            'storage_sum_files' => Module::t('storage-settings', 'Number of compressed files'),
            'storage_sum_size' => Module::t('storage-settings', 'Spots saved'),
            'status_squeeze' => Module::t('storage-settings', 'Compression on loading'),
        ];
    }
}
