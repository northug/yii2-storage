<?php

namespace northug\storage\models\unload;

use Yii;
use yii\base\Model;
use claviska\SimpleImage;

/**
 * Description of MStorage
 *
 * @author Пользователь
 */
class MStorage extends Model {

    public $id;
    public $path;
    public $absolutePath;
    public $originalPath;
    public $alt;
    public $old_name;
    public $type_file;
    public $width;
    public $height;
    public $size;
    public $squeeze;
    public $upload_ip;
    public $expansion;
    public $updated_at;
    public $created_at;

    public function rules() {
        return [
            [['absolutePath', 'id', 'path', 'alt', 'old_name', 'type_file', 'width', 'height', 'size', 'squeeze', 'upload_ip', 'expansion', 'updated_at', 'created_at'], 'safe']
        ];
    }
    
    public function getFullPath() {
        $currentHost = Yii::$app->request->headers->get('host');
        $host = substr($currentHost, strpos($currentHost, ".") + 1, strlen($currentHost));
        $homeUrl = Yii::$app->modules['storage'] ? Yii::$app->modules['storage']->homeUrl : null;
        return $homeUrl ? $homeUrl . $this->path : 'http://' . $host . $this->path;
    }

    /**
     * 
     * @param type $class
     * @return \northug\storage\models\FileModification
     */
    public function image($class = '\northug\storage\models\FileModification') {
        return new $class($this);
    }

    public function simpleImage($path = null) {
        if ($path === null) {
            return new SimpleImage(Yii::getAlias($this->pathAbsolute . $this->path));
        }
        return new SimpleImage($path);
    }

}
