<?php

namespace northug\storage\models\unload;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use northug\storage\models\Storage;
use northug\storage\models\unload\MStorage;
use northug\storage\models\StorageToModel;

/**
 * Description of StorageUnload
 *
 * @author Пользователь
 */
class StorageUnload extends Model {

    private $model;

    public function getFile($model, $attribute, $unloadStatus = false) {
        $this->model = new MStorage();
        if (!$model->$attribute or $unloadStatus or is_array($model->$attribute)) {
            if ($file = $this->getFileStorage($model, $attribute)) {
                $this->model->attributes = $file->attributes;
                $this->model->originalPath = $this->model->path;
                unset($file);
                return $this->model;
            }
        }
        return $model->$attribute;
    }

    public function getFiles($model, $attribute, $unloadStatus = false) {
        if (!$model->$attribute or $unloadStatus or !is_array($model->$attribute)) {
            $files = $this->getFilesStorage($model, $attribute);
            $listFiles = [];
            foreach ($files as $file) {
                $model = new MStorage();
                $model->attributes = $file->attributes;
                $model->originalPath = $model->path;
                $listFiles[] = $model;
                unset($model);
            }
            unset($files);
            return $listFiles;
        }
        return $model->$attribute;
    }
    
    private function getFileStorage($model, $attribute) {
        $file = $this->searchToModel($model, $attribute)->one();
        return $file ? $file->file : null;
    }

    private function getFilesStorage($model, $attribute) {
        $files = $this->searchToModel($model, $attribute)->all();
        return ArrayHelper::getColumn($files, 'file');
    }

    /**
     * 
     * @param yii\base\Model $model
     * @param string $attribute
     * @return yii\db\ActiveQuery
     */
    private function searchToModel($model, $attribute) {
        return StorageToModel::find()
                        ->where([
                            'model' => $this->getModelName($model),
                            'attribute' => $attribute,
                            'model_id' => $model->id,
                        ])
                        ->with(['file'])
                        ->orderBy(['sort' => SORT_ASC]);
    }

    /**
     * 
     * @param yii\base\Model $model
     * @return string
     */
    private function getModelName($model) {
        return $model->formName();
    }

}
