<?php

namespace northug\storage\models;

use northug\storage\models\unload\MStorage;

/**
 * Description of Load
 *
 * @author Пользователь
 */
class Load {

    /**
     * 
     * @param \yii\base\Model $models[]
     */
    public static function getFiles(&$models, $attributes, $modelName = null) {
        if (!$models) {
            return;
        }
        
        $modelsIDs = is_array($models) ? static::getModelsIDs($models) : $models->id;
        
        if ($modelName === null) {
            $modelName = is_array($models) ? $models[0]->formName() : $models->formName();
        }

        $res = StorageToModel::find()
                        ->with(['file'])
                        ->where([
                            'model' => $modelName,
                            'attribute' => $attributes,
                            'model_id' => $modelsIDs,
                        ])->orderBy(['sort' => SORT_ASC])->asArray()->all();

        foreach ($res as $item) {
            if (is_array($models)) {
                foreach ($models as &$model) {
                    if (isset($model->id) and property_exists($model, $item['attribute']) and $model->id == $item['model_id']) {
                        $mstorage = new MStorage(['originalPath' => $item['file']['path']]);
                        $mstorage->attributes = $item['file'];
                        $model->{$item['attribute']}[] = $mstorage;
                        break;
                    }
                }
            } else {
                if (isset($models->id) and property_exists($models, $item['attribute']) and $models->id == $item['model_id']) {
                    $mstorage = new MStorage(['originalPath' => $item['file']['path']]);
                    $mstorage->attributes = $item['file'];
                    $models->{$item['attribute']}[] = $mstorage;
                }
            }
        }
    }
    
    /**
     * 
     * @param \yii\base\Model $models[]
     */
    public static function getFile(&$models, $attributes, $modelName = null) {
        $modelsIDs = is_array($models) ? static::getModelsIDs($models) : $models->id;
        
        if ($modelName === null) {
            $modelName = is_array($models) ? $models[0]->formName() : $models->formName();
        }

        $res = StorageToModel::find()
                        ->with(['file'])
                        ->where([
                            'model' => $modelName,
                            'attribute' => $attributes,
                            'model_id' => $modelsIDs,
                        ])->orderBy(['sort' => SORT_ASC])->asArray()->all();

        foreach ($res as $item) {
            if (is_array($models)) {
                foreach ($models as &$model) {
                    if (isset($model->id) and property_exists($model, $item['attribute']) and $model->id == $item['model_id'] and $model->{$item['attribute']} === null) {
                        $model->{$item['attribute']} = new MStorage(['originalPath' => $item['file']['path']]);
                        $model->{$item['attribute']}->attributes = $item['file'];
                        break;
                    }
                }
            } else {
                if (isset($models->id) and property_exists($models, $item['attribute']) and $models->id == $item['model_id']) {
                    $mstorage = new MStorage(['originalPath' => $item['file']['path']]);
                    $mstorage->attributes = $item['file'];
                    $models->{$item['attribute']}[] = $mstorage;
                    break;
                }
            }
        }
    }
    
    private static function getModelsIDs($models) {
        $modelsIDs = [];
        foreach ($models as $model) {
            if (isset($model->id)) {
                $modelsIDs[] = $model->id;
            }
        }
        return $modelsIDs;
    }
    
}
