<?php

namespace northug\storage\models;

use Yii;

/**
 * This is the model class for table "storage_to_category".
 *
 * @property int $id
 * @property int $storage_id
 * @property int $category_id
 */
class StorageToCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'storage_to_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['storage_id', 'category_id'], 'required'],
            [['storage_id', 'category_id'], 'integer'],
        ];
    }
    
    /**
     * 
     * @param integer $storageId
     * @param integer $categoryId
     * @return type
     */
    public static function getOne(int $storageId, int $categoryId)
    {
        return static::find()->where([
            'storage_id' => $storageId,
            'category_id' => $categoryId,
        ])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storage_id' => 'Storage ID',
            'category_id' => 'Category ID',
        ];
    }
}
