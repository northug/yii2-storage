<?php

namespace northug\storage\models;

use Yii;

/**
 * This is the model class for table "storage_resize_files".
 *
 * @property int $id
 * @property string $absolutePath
 * @property string $hash
 * @property int $squeeze
 */
class StorageResizeFiles extends \yii\db\ActiveRecord
{
    
    const NOT_COMPRESS = 0, SUCCESS_COMPRESS = 1;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'storage_resize_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['absolutePath', 'hash'], 'required'],
            [['hash'], 'unique'],
            [['absolutePath'], 'string'],
            [['squeeze'], 'integer'],
            [['hash'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'absolutePath' => 'Absolute Path',
            'hash' => 'Hash',
            'squeeze' => 'Squeeze',
        ];
    }
}
