<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace northug\storage\models\form;

use yii\base\Model;
use northug\storage\behaviors\FileUploadBehavior;

/**
 * Description of FileForm
 *
 * @author Пользователь
 */
class FileForm extends Model {

    public $files;
    public $maxFiles = 100;

    public function rules() {
        return [
            //['files', 'required'],
            [['files'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => $this->maxFiles
            ]
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => FileUploadBehavior::className(),
                'attributes' => ['files'],
            ],
        ];
    }

    public function attributeLabels() {
        return [
            'files' => 'Файл(ы)',
        ];
    }

}
