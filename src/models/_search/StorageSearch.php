<?php

namespace northug\storage\models\_search;

use Yii;
use yii\base\Model;
use northug\storage\Module;
use yii\data\ActiveDataProvider;
use northug\storage\models\Storage;
use northug\storage\models\Categories;
use northug\storage\models\StorageToCategory;

/**
 * StorageSearch represents the model behind the search form of `northug\storage\models\Storage`.
 */
class StorageSearch extends Storage
{
    public $after_size;
    public $before_size;
    
    public $after_created_at;
    public $before_created_at;
    
    public $categories;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'width', 'height', 'size', 'squeeze', 'updated_at', 'created_at'], 'integer'],
            [['after_size', 'before_size'], 'number'],
            [['path', 'absolutePath', 'alt', 'old_name', 'expansion', 'upload_ip', 'categories', 'after_created_at', 'before_created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Storage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($this->categories) {
            $categoriesIDs = Categories::find()->select('id')->where(['status' => Categories::STATUS_ACTIVE, 'name' => $this->categories])->column();
            $storageIDs = StorageToCategory::find()->select('storage_id')->where(['category_id' => $categoriesIDs])->column();
            if ($storageIDs) {
                $query->andWhere(['id' => $storageIDs]);
            } else {
                $query->where('0=1');
            }
        }
        
        if ($this->after_created_at) {
            $this->after_created_at = strtotime($this->after_created_at);
        }
        
        if ($this->before_created_at) {
            $this->before_created_at = strtotime($this->before_created_at)+86400;
        }
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if ($this->after_size) {
            $this->after_size *= 1024*1024;
            $query->andWhere(['>', 'size', $this->after_size]);
        }
        
        if ($this->before_size) {
            $this->before_size *= 1024*1024;
            $query->andWhere(['<', 'size', $this->before_size]);
        }
        
        if ($this->after_created_at) {
            $query->andWhere(['>=', 'created_at', $this->after_created_at]);
        }
        
        if ($this->before_created_at) {
            $query->andWhere(['<=', 'created_at', $this->before_created_at]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'width' => $this->width,
            'height' => $this->height,
            'size' => $this->size,
            'squeeze' => $this->squeeze,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'path', $this->path])
            ->andFilterWhere(['like', 'absolutePath', $this->absolutePath])
            ->andFilterWhere(['like', 'alt', $this->alt])
            ->andFilterWhere(['like', 'old_name', $this->old_name])
            ->andFilterWhere(['like', 'expansion', $this->expansion])
            ->andFilterWhere(['like', 'upload_ip', $this->upload_ip]);

        
        if (!Yii::$app->request->get('sort')) {
            $query->orderBy(['id' => SORT_DESC]);
        }
        
        return $dataProvider;
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'after_size' => Module::t('storage-filter', 'Size from (in Mb)'),
            'before_size' => Module::t('storage-filter', 'Size up to (in Mb)'),
            'id' => 'ID',
            'path' => Module::t('storage', 'Path'),
            'absolutePath' => Module::t('storage', 'Absolute path to the root of the site'),
            'old_name' => Module::t('storage', 'Old Name'),
            'alt' => Module::t('storage', 'Alt'),
            'expansion' => Module::t('storage', 'Expansion'),
            'width' => Module::t('storage', 'Width'),
            'height' => Module::t('storage', 'Height'),
            'size' => Module::t('storage', 'Size'),
            'squeeze' => Module::t('storage', 'Squeeze'),
            'upload_ip' => Module::t('storage', 'Upload Ip'),
            'updated_at' => Module::t('storage', 'Updated At'),
            'created_at' => Module::t('storage', 'Created At'),
            'categories' => Module::t('storage-filter', 'Categories'),
            'after_created_at' => Module::t('storage-filter', 'Date added from'),
            'before_created_at' => Module::t('storage-filter', 'Uploaded to'),
        ];
    }
    
}