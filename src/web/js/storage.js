$('body').on('click', '.list-files .delete-file', function () {
    $(this).parent().parent().remove();
});

$('body').on('click', '.open-library', function () {
    var popup = $('#popup-library');
    var attribute = $(this).data('attribute');
    var name = $(this).data('name');
    $('.add-library').attr('data-attribute', attribute);
    $('.add-library').attr('data-name', name);
    if (popup.hasClass('active')) {
        popup.removeClass('active');
    } else {
        loadPage(attribute);
        popup.addClass('active');
    }
});

$('body').on('click', '.close-library', function () {
    var popup = $('#popup-library');
    if (popup.hasClass('active')) {
        popup.removeClass('active');
    }
    return false;
});

function loadPage(attribute) {
    $.ajax({
        url: '/storage/library/load',
        type: 'get',
        dataType: 'html',
        data: 'attribute='+attribute,
        success: function (res) {
            $('#popup-library .list-files-popup').html(res);
            return false;
        },
    });
}

var selectFiles = [];

$('body').on('change', '#popup-library .file-item input[type=checkbox]', function () {
    var idFile = $(this).parent().data('id');
    if (selectFiles.indexOf(idFile) != -1) {
        selectFiles.splice(selectFiles.indexOf(idFile), 1);
    } else {
        selectFiles.push(idFile);
    }
});

$('body').on('click', '#popup-library .add-library', function () {
    var data = JSON.stringify(selectFiles);
    var name = $(this).data('name');
    var attribute = $(this).data('attribute');
    
    var url = '/storage/library/add';
    var data = 'files='+data+'&attribute='+attribute+'&name='+name;
    if (!$('ul').is('#'+attribute+'-sortable')) {
        data += '&sortable=true';
    }
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'html',
        data: data,
        success: function (res) {
            if ($('ul').is('#'+attribute+'-sortable')) {
                $('#'+attribute+'-sortable').append(res);
            } else {
                $('.file-widget-block[data-attribute='+attribute+']').append(res);
            }
            $('#popup-library').removeClass('active');
            return false;
        },
    });
    return false;
});

$('body').on('click', '#popup-library .pagination a', function () {
    var href = $(this).attr('href');
    $.ajax({
        url: href,
        type: 'post',
        dataType: 'html',
        success: function (res) {
            $('#popup-library .list-files-popup').html(res);
            return false;
        },
    });
    return false;
});

$('body').on('click', 'button.storage-search', function () {
    var data = $('#filter-storage-form :input').serialize();
    var attribute = $('.add-library').data('attribute');
    $.ajax({
        url: '/storage/library/load',
        type: 'get',
        dataType: 'html',
        data: data+'&attribute='+attribute,
        success: function (res) {
            $('#popup-library .list-files-popup').html(res);
            return false;
        },
    });
    return false;
});

$('body').on('click', '.open-filter-in-storage', function () {
    
    var openHtml = $(this).data('open');
    var closeHtml = $(this).data('close');
    
    if ($(this).hasClass('close-filter')) {
        $(this).html(closeHtml);
        $(this).removeClass('close-filter');
        $(this).addClass('open');
        $('.storage-search').removeClass('hide-storage-search');
    } else {
        $(this).html(openHtml);
        $(this).removeClass('open');
        $(this).addClass('close-filter');
        $('.storage-search').addClass('hide-storage-search');
    }
    return false;
});
